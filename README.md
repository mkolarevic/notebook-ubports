# Notebook

A note taking application for [UBPorts](https://ubports.com/). Built with Vite + SvelteJS.

## Features

**Notebook** has additional features not found in simpler note taking apps.

### Adaptive UI

UI adaptive to all screen sizes. This will ensure a good user experience on various displays.

### Labels

Categorize your notes with labels. This will allow you to easily find and manage thousands of notes.

### Export notes

Export your notes in a markdown, plain text or JSON format. This will let you back up your notes onto other devices.

### Simple interface

Notebook uses the familiar Material UI with an Ubuntu-inspired theme which will make you feel at home.

### Localizations

With over a dozen languages localized the application is made accessible to a great number of users.

## Development

### Svelte (HTML5)

The native HTML5 app is developed just like any other SvelteJS application. The Svelte application is within the `src/` directory.

The `@vitejs/plugin-legacy` dependency is required to ensure compatibility with older web renderers. In this project's case, the target browser is Chrome 87, as that is what the UBPorts web renderer uses (in Ubuntu v20.04 base). The `vite-legacy-plugin` will polyfill all JavaScript functionality, meaning you can use modern features when writing code. However, modern CSS features cannot be polyfilled and CSS feature compatibility must be checked at [CanIUse](https://caniuse.com/).

To build the Svelte application, first navigate to the `src` directory and install dependencies. Then, within the `src/` directory, run the `build` script to build the Svelte application and then run the `export` script to move the contents of the `src/dist/` directory to the `app/www` directory.

1. `cd ./src`
2. `bun i` or `npm i`
3. `bun run build` or `npm run build`
4. `bun run export` or `npm run export`

### Clickable

Once you've built the Svelte application and have updated the contents of the `www/` directory, you can build the Clickable application for UBPorts. The Clickable application can be found in the `app/` directory.

To build the app run `clickable build` (inside the `/app` directory). Once the application is built, you can install it to your device via ADB by running `clickable install` (inside the `/app` directory). Finally, you can launch the application on your phone by running `clickable launch` or by navigating to the app manually.

### Building the application

The `buildall.sh` script will automate the process of building the Svelte app, exporting the built app and building the Clickable package. After the script finishes executing, you can run the `install.sh` script to push the app to your phone via ADB. You need to authorize adb access to your PC before pushing a Clickable package to your phone; your phone will usually prompt you to do this when you run `adb devices` (or any other `adb` command) while your phone is plugged in.

Note that [`bun`](https://bun.sh/) is the default package manager used by the `buildall.sh` script. If you wish to use a different package manager you can do it by passing it as an argument (for example: `./buildall.sh pnpm`).

## Test the application

You can check how the app works on the [demo page](https://mkolarevic.gitlab.io/notebook-ubports/).

## Screenshots

![App screenshot - mobile layout](./repo_assets/images/mobile_note.png)

![App screenshot - desktop layout](./repo_assets/images/desktop_note.png)

## License

Copyright (c) 2024 Matej Kolarević

Licensed under the MIT license.
