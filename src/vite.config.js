import { svelte } from '@sveltejs/vite-plugin-svelte';
import legacy from '@vitejs/plugin-legacy';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
	build: {
		cssTarget: ['chrome87'],
		cssMinify: 'lightningcss',
	},
	base: './',
	plugins: [svelte(),
	legacy({
		targets: ['chrome 87']
	})],
})
