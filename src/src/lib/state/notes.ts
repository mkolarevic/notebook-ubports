import Fuse from "fuse.js";
import { get, writable } from "svelte/store";
import { formatDate } from "../utils/dateTime";
import type {
  NoteExportFormat,
  NoteObject,
  SortByOptions,
} from "../utils/types";
import { getStorageItem, setStorageItem } from "./storage";

/* Private */
/** Loads notes from persistent storage */
function _loadNotes() {
  const _NotesList = getStorageItem<NoteObject[]>("NotesList") ?? [];

  NotesList.set(_NotesList);
}

/**
 * Stores notes into persistent storage
 *
 * @param noteList
 * The array of notes to store
 */
function _storeNotes(noteList: NoteObject[]) {
  setStorageItem("NotesList", noteList);
}

/** Loads note labels from persistent storage */
function _loadLabels() {
  const _NoteLabels = getStorageItem<string[]>("NoteLabels") ?? [];

  NoteLabels.set(_NoteLabels);
}

/**
 * Stores labels into persistent storage
 *
 * @param labels
 * The array of labels to store
 */
function _storeLabels(labels: string[]) {
  setStorageItem("NoteLabels", labels);
}

/**
 * Returns a string representing a date in ISO format
 *
 * @param time
 * UNIX time
 */
function _date(time?: number) {
  if (!time) {
    return new Date().toISOString().split("T")[0];
  }

  return new Date(time).toISOString().split("T")[0];
}

/**
 * Returns the title of the provided note. If the title is empty, the `createdAt` date string will be returned instead
 *
 * @param note
 * Note object
 */
function _getNoteTitle(note: NoteObject) {
  if (!note.title.trim()) {
    return `(${_date(note.createdAt)})`;
  }

  return note.title;
}

/**
 * Downloads the provided data onto the device
 *
 * @param data
 * Data to download
 * @param filename
 * The downloaded file name
 */
function _download(data: string, filename: string) {
  const a = document.createElement("a");
  let file = new Blob([data], {
    type: "text/plain",
  });
  a.href = URL.createObjectURL(file);
  a.download = filename;
  a.click();
}

/**
 * Returns an extension for the provided file type. `json` will be returned for unsupported file types.
 * @param filetype
 * File type
 * @returns
 */
function _getExt(filetype: NoteExportFormat) {
  return (
    {
      json: "json",
      text: "txt",
      markdown: "md",
    }[filetype] ?? "json"
  );
}

/* Actions - Notes */
/** Deletes all notes, including the persistent storage */
export function deleteAllNotes() {
  NotesList.update(() => []);
  _storeNotes([]);
}

/**
 * Deletes a note by UUID
 *
 * @param uuid
 * Note UUID
 */
export function deleteNote(uuid: string) {
  const _deletedNote = getNoteById(uuid);
  _deletedNote.deleted = true;
  updateNote(_deletedNote);
}

/**
 * Updates all properties of an individual Note
 *
 * @param noteData
 * Complete Note object
 * @param initialNoteData
 * Optional parameter used when updating `updatedAt` value
 * @param final
 * Marks note as deleted if it's empty
 */
export function updateNote(
  noteData: NoteObject,
  initialNoteData?: NoteObject,
  final?: boolean
) {
  if (!noteData.title && !noteData.content && final) {
    noteData.deleted = true;
  }

  if (noteData.deleted) {
    NotesList.update((noteList) => {
      const _newList = noteList.filter((_note) => !_note.deleted);
      _storeNotes(_newList);

      return _newList;
    });

    return;
  }

  NotesList.update((noteList) => {
    const index = noteList.findIndex((_note) => _note.uuid === noteData.uuid);

    if (index < 0) {
      /* Add a new note */
      noteList.push(noteData);
    } else {
      /* Update existing note */
      if (initialNoteData) {
        let _hasChanged = false;
        ["content", "title"].forEach((property: "content" | "title") => {
          if (initialNoteData[property] !== noteList[index][property]) {
            _hasChanged = true;
          }
        });
        if (_hasChanged) {
          noteData.updatedAt = new Date().valueOf();
        }
      }
      noteList[index] = noteData;
    }

    _storeNotes(noteList);

    return noteList;
  });
}

/**
 * Updates all properties of the passed list of notes
 *
 * @param newNoteData
 * The full list of notes to be updated
 */
export function bulkUpdateNote(newNoteData: NoteObject[]) {
  NotesList.update((noteList) => {
    newNoteData.forEach((_note) => {
      const index = noteList.findIndex((_n) => _n.uuid === _note.uuid);

      /* If we don't find a note by ID, we skip it */
      if (index >= 0) {
        let _hasChanged = false;

        ["content", "title"].forEach((property: "content" | "title") => {
          if (_note[property] !== noteList[index][property]) {
            _hasChanged = true;
          }
        });

        if (_hasChanged) {
          _note.updatedAt = new Date().valueOf();
        }

        noteList[index] = _note;
      }
    });

    _storeNotes(noteList);

    return noteList;
  });
}

/**
 * Toggles the `pinned` property of a note
 *
 * @param uuid
 * Note UUID
 */
export function pinNote(uuid: string) {
  if (!uuid) return;

  const index = get(NotesList).findIndex((_note) => _note.uuid === uuid);
  if (index < 0) return;

  const noteData = get(NotesList)[index];
  noteData.pinned = !noteData.pinned;

  updateNote(noteData);
}

/**
 * Changes the `archived` property of a note
 *
 * @param uuid
 * Note UUID
 * @param isArchived
 * The new `archived` state
 */
export function toggleArchivedState(uuid: string, isArchived: boolean) {
  if (!uuid) return;

  const index = get(NotesList).findIndex((_note) => _note.uuid === uuid);

  if (index < 0) return;

  const noteData = get(NotesList)[index];

  noteData.archived = !!isArchived;

  updateNote(noteData);
}

/**
 * Updates the list of labels in a note
 *
 * @param noteData
 * The note to be updated
 * @param labels
 * The new list of labels
 */
export function modifyNoteLabels(noteData: NoteObject, labels: string[]) {
  noteData.labels = labels;

  updateNote(noteData);
}

/* Action - Labels */
/**
 * Creates a label and stores it in persistent storage
 *
 * @param label
 * The label to create
 */
export function addLabel(label: string) {
  NoteLabels.update((currentList) => {
    const _list = currentList.filter((_label) => label !== _label);
    _list.unshift(label);
    return _list;
  });

  _storeLabels(get(NoteLabels));
}

/**
 * Deletes a label, deleting it from all notes.
 * Returns a list of affected notes.
 *
 * @param label
 * Label to delete
 */
export function deleteNoteLabel(label: string) {
  NoteLabels.update((currentList) => {
    return currentList.filter((_label) => label !== _label);
  });

  // Remove the specified label from all notes
  const updatedNotes: NoteObject[] = [];
  const allNotes = get(NotesList);

  for (let i = 0; i < allNotes.length; i++) {
    if (allNotes[i].labels.includes(label)) {
      allNotes[i].labels = allNotes[i].labels.filter(
        (_label) => label !== _label
      );
      updatedNotes.push(allNotes[i]);
    }
  }

  bulkUpdateNote(updatedNotes);
  _storeLabels(get(NoteLabels));

  return updatedNotes;
}

/**
 * "Renames" a label.
 * The original label is deleted and a new label is created to replace it.
 * Notes which had the original label will be updated to include the new one.
 *
 * @param oldLabel
 * Label to rename
 * @param newLabel
 * New label name
 */
export function updateNoteLabel(oldLabel: string, newLabel: string) {
  if (!oldLabel || !newLabel || oldLabel?.trim() === newLabel?.trim()) {
    return;
  }

  const notesToUpdate = deleteNoteLabel(oldLabel);

  addLabel(newLabel);

  for (let i = 0; i < notesToUpdate.length; i++) {
    notesToUpdate[i].labels.push(newLabel);
  }
  bulkUpdateNote(notesToUpdate);
}

/* State */

export const NotesList = writable<NoteObject[]>([]);

/**
 * Returns a single note based on it's UUID
 *
 * @param uuid
 * Note UUID
 */
export function getNoteById(uuid: string) {
  return get(NotesList).find((_note) => _note.uuid === uuid);
}

/**
 * Returns a list of notes based on the archived status
 *
 * @param isArchived
 * Note's archived status. `true` or `"archived"` is an archived note, `false` or `"notes"` is a non-archived note.
 * @returns
 */
export function getNoteByArchivedState(
  isArchived: boolean | "archived" | "notes"
) {
  if (typeof isArchived !== "boolean") {
    isArchived = isArchived === "archived";
  }

  return get(NotesList).filter((_note) => {
    return isArchived === !!_note.archived;
  });
}

export const searchQuery = writable("");

export const noteGroup = writable<"notes" | "archived">("notes");

export const NoteLabels = writable<string[]>([]);

/* Utils */
/**
 * Sorts provided notes. Pinned notes will always take precedence.
 *
 * @param Notes
 * The list of notes to sort
 * @param inverseSortOrder
 * Determines if the order of the returned notes should be in ascending or descending order
 * @param sortBy
 * Determines which property the notes should be sorted by
 * @returns
 */
export function sortedNotes(
  Notes: NoteObject[],
  inverseSortOrder: boolean,
  sortBy: SortByOptions
): NoteObject[] {
  const _notes = Notes;
  const _order = inverseSortOrder ? 1 : -1;

  _notes.sort((a: NoteObject, b: NoteObject) => {
    if (a[sortBy] > b[sortBy]) return _order;
    if (a[sortBy] < b[sortBy]) return _order * -1;
    return 0;
  });

  const _pinned = _notes.filter((_note) => _note.pinned);
  const _rest = _notes.filter((_note) => !_note.pinned);

  return [..._pinned, ..._rest];
}

/**
 * Uses fuzzy search to filter the notes based on the search query.
 *
 * @param Notes
 * The list of notes to filter
 * @param searchQuery
 * Search query
 * @param locale
 * Determines which locale should be used to search dates
 */
export function filteredNotes(
  Notes: NoteObject[],
  searchQuery: string,
  locale: string
): NoteObject[] {
  const _notes = Notes;
  const fuse = new Fuse(
    _notes.map((_n) => {
      return {
        ..._n,
        _createdAt: formatDate({ date: _n.createdAt, locale, relative: false }),
      };
    }),
    {
      keys: ["title", "content", "labels", "_createdAt"],
    }
  );

  const _res = fuse.search(searchQuery);

  return _res.map((item) => {
    const r = item.item;
    const { _createdAt, ...rest } = r;
    return rest;
  });
}

/**
 * Returns a list of notes mapped for a specific screen. Notes are filtered by archived state and search query.
 * If a search query is not provided, notes are sorted instead.
 * @param inverseSortOrder
 * Determines if the order of the returned notes should be in ascending or descending order
 * @param sortBy
 * Determines which property the notes should be sorted by
 * @param locale
 * Determines which locale should be used to search dates
 */
export function mappedNotes(
  inverseSortOrder: boolean,
  sortBy: SortByOptions,
  locale: string
) {
  const _noteGroup = getNoteByArchivedState(get(noteGroup));
  const _filtered = get(searchQuery)
    ? filteredNotes(_noteGroup, get(searchQuery), locale)
    : _noteGroup;

  if (!get(searchQuery)) {
    return sortedNotes(_noteGroup, inverseSortOrder, sortBy);
  }

  return _filtered;
}

/**
 * Exports all notes in the specified format
 *
 * @param format
 * Determines which file format the notes should be exported in
 */
export function exportNotes(format: NoteExportFormat) {
  /* Generate the file name */
  const _ext = _getExt(format);

  const _fileName = `notes-${_date()}.${_ext}`;

  /* Get data */
  let _data = "";

  if (format === "json") {
    _data = JSON.stringify(get(NotesList));
  }

  if (format === "text") {
    _data = get(NotesList)
      .map((note) => {
        return [_getNoteTitle(note), note.content].join("\n\n");
      })
      .join("\n\n\n----------------\n");
  }

  if (format === "markdown") {
    const _head = `# Notes (${_date()})`;
    const _main = get(NotesList)
      .map((note) => {
        return [`## ${_getNoteTitle(note)}`, note.content].join("\n\n");
      })
      .join("\n\n");
    _data = [_head, _main].join("\n\n");
  }

  /* Download */
  _download(_data, _fileName);
}

/**
 * Exports a single note in the specified format
 *
 * @param format
 * Determines which file format the note should be exported in
 * @param uuid
 * UUID of the note to export
 */
export function exportNote(format: NoteExportFormat, uuid: string) {
  const _note = get(NotesList).find((note) => note.uuid === uuid);

  /* Generate the file name */
  const _ext = _getExt(format);

  const _fileName = `note-${_date()}-${_note.title.replace(
    /[^a-zA-Z0-9-]/g,
    ""
  )}.${_ext}`;
  /* Get data */
  let _data = "";

  if (format === "json") {
    _data = JSON.stringify(_note);
  }

  if (format === "text") {
    _data = [_note.title, _note.content].join("\n\n");
  }

  if (format === "markdown") {
    _data = [`# ${_note.title}`, _note.content].join("\n\n");
  }

  /* Download */
  _download(_data, _fileName);
}

/* Init */

_loadNotes();
_loadLabels();
