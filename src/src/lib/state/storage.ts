import type { AppSettingsType } from "../utils/types";

/** Returns AppSettings in JSON format */
export function getAppSettings(): AppSettingsType {
  return JSON.parse(localStorage.getItem("AppSettings")) ?? {};
}

/** Stores AppSettings */
export function setAppSettings(AppSettings: AppSettingsType) {
  localStorage.setItem("AppSettings", JSON.stringify(AppSettings));
}

export function setStorageItem<T>(item: string, value: T) {
  localStorage.setItem(item, JSON.stringify(value));
}

export function getStorageItem<T>(item: string) {
  return JSON.parse(localStorage.getItem(item)) as T | undefined;
}
