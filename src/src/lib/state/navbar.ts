import { get, writable } from "svelte/store";
import { generateUUID } from "../utils/uuid";
import { NotesList, noteGroup } from "./notes";
import { router } from "./router";

export const nav = {
  noteID: writable(""),
  noteData() {
    return get(NotesList).find((note) => note.uuid === get(nav.noteID));
  },
  getFab() {
    if (!["", "#"].includes(get(router.path))) {
      return {
        icon: "arrow_back",
        action() {
          router.setPath("");
          noteGroup.set("notes");
        },
      };
    }
    return {
      icon: "add",
      action() {
        router.setPath(`note_${generateUUID()}`);
      },
    };
  },
  _getNoteID() {
    return get(router.path)?.split("_")?.[1] ?? "";
  },
};

router.path.subscribe(() => {
  nav.noteID.set(nav._getNoteID());
});
