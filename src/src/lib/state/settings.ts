import { get, writable } from "svelte/store";
import { changeLocale } from "../utils/locales";
import type { AppSettingsType, SortByType } from "../utils/types";
import { getAppSettings, setAppSettings } from "./storage";

/** Loads the stored app settings */
function _loadSettings() {
  const AppSettings = getAppSettings();

  appTheme.set(AppSettings.appTheme ?? "system");
  baseFontSize.set(AppSettings.baseFontSize ?? "normal");
  notesPerPage.set(AppSettings.notesPerPage ?? 25);
  dateTimeFormat.set(AppSettings.dateTimeFormat ?? "absolute");
  if (AppSettings.locale) {
    changeLocale(AppSettings.locale);
  }

  setFontSize(get(baseFontSize));
  setTheme(get(appTheme));
  setDateTimeFormat(get(dateTimeFormat));

  if (typeof AppSettings.inverseSortOrder === "boolean") {
    inverseSortOrder.set(AppSettings.inverseSortOrder);
  }

  if (typeof AppSettings.sortBy === "string") {
    sortBy.set(AppSettings.sortBy);
  }
}

const _sortByMap = Object.freeze({
  createdAt: "updatedAt",
  updatedAt: "viewedAt",
  viewedAt: "createdAt",
});

/* Actions - Theme */
/**
 * Sets the application theme
 *
 * @param theme
 * Theme to apply
 */
export function setTheme(theme: AppSettingsType["appTheme"]) {
  const AppSettings = getAppSettings();

  AppSettings.appTheme = theme;
  appTheme.set(theme);

  try {
    const _light = document.getElementById("LightModeCSS");
    const _dark = document.getElementById("DarkModeCSS");
    const _lightSelector = "screen and (prefers-color-scheme: light)";
    const _darkSelector = "screen and (prefers-color-scheme: dark)";
    const _enabledSelector = "screen";
    const _disabledSelector =
      "screen and (prefers-color-scheme: dark) and (prefers-color-scheme: light)";

    //@ts-ignore
    _dark.media = {
      system: _darkSelector,
      dark: _enabledSelector,
      light: _disabledSelector,
    }[AppSettings.appTheme];

    //@ts-ignore
    _light.media = {
      system: _lightSelector,
      dark: _disabledSelector,
      light: _enabledSelector,
    }[AppSettings.appTheme];

    setAppSettings(AppSettings);
  } catch (error) {}
}

/* Actions - Sort */
/** Changes the sort order of the notes */
export function changeSortOrder() {
  const AppSettings = getAppSettings();
  AppSettings.inverseSortOrder = !get(inverseSortOrder);
  inverseSortOrder.set(AppSettings.inverseSortOrder);
  setAppSettings(AppSettings);
}
/** Changes the parameter used to sort notes, between `createdAt`, `editedAt` and `viewedAt` */
export function changeSortBy() {
  const AppSettings = getAppSettings();
  AppSettings.sortBy = _sortByMap[get(sortBy)];
  sortBy.set(AppSettings.sortBy);
  setAppSettings(AppSettings);
}

/* Actions - Font size */
/** Sets the application font size */
export function setFontSize(fontSize: AppSettingsType["baseFontSize"]) {
  const AppSettings = getAppSettings();
  AppSettings.baseFontSize = fontSize;
  baseFontSize.set(fontSize);
  try {
    const htmlTag = document.getElementsByTagName("html")?.[0];
    htmlTag.style.fontSize =
      {
        small: "87.5%",
        normal: "100%",
        large: "112.5%",
        extraLarge: "125%",
      }[fontSize] ?? "100%";
    setAppSettings(AppSettings);
  } catch (error) {}
}

/* Actions - Page size */
/** Sets the number of notes/labels displayed per page */
export function setPageSize(_pageSize: AppSettingsType["notesPerPage"]) {
  const AppSettings = getAppSettings();
  AppSettings.notesPerPage = _pageSize;
  notesPerPage.set(_pageSize);
  setAppSettings(AppSettings);
}

/* Actions - DateTime format */
/** Sets the date-time format */
export function setDateTimeFormat(_format: AppSettingsType["dateTimeFormat"]) {
  const AppSettings = getAppSettings();
  AppSettings.dateTimeFormat = _format;
  dateTimeFormat.set(_format);
  setAppSettings(AppSettings);
}

/* State */

export const appTheme = writable<AppSettingsType["appTheme"]>("system");
export const sortBy = writable<SortByType>("createdAt");
export const inverseSortOrder = writable<boolean>(false);
export const baseFontSize = writable<AppSettingsType["baseFontSize"]>("normal");
export const notesPerPage = writable<AppSettingsType["notesPerPage"]>(25);
export const dateTimeFormat =
  writable<AppSettingsType["dateTimeFormat"]>("absolute");

/* Init */

_loadSettings();
