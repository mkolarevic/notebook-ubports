import { writable } from "svelte/store";

/** Stores relevant router information */
export const router = {
  path: writable(""),
  setPath(href?: string) {
    window.location.hash = href ?? "";
  },
  _updatePath(hash?: string) {
    router.path.update(() => hash ?? "");
  },
};

/** Automatically updates the path based on the current route */
window.addEventListener("hashchange", () => {
  router._updatePath(window.location.hash);
});

/** Preserve location when reloading */
router._updatePath(window.location.hash);
