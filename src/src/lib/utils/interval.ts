/**
 * Debouncer class used for triggering functions after a delay
 */
export class Debouncer {
  public timerId?: NodeJS.Timeout;
  public callback?: Function;

  constructor(callback: Function) {
    this.callback = callback;
  }

  clear() {
    clearTimeout(this.timerId);
  }

  run(delay = 3000) {
    this.clear();

    if (typeof this.callback === "function") {
      this.timerId = setTimeout(() => {
        this.callback();
      }, delay);
    }
  }
}
