/** Generates a UUID to be used for identifying notes */
export function generateUUID() {
  if (typeof crypto?.randomUUID == "function") {
    return crypto.randomUUID();
  }

  if (typeof crypto?.getRandomValues == "function") {
    const _tempArray = new Uint32Array(1);
    crypto.getRandomValues(_tempArray);

    return `${_tempArray[0]}`;
  }

  return `${Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)}`;
}
