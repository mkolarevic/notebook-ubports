/** Scrolls to the bottom of the page */
export function scrollToBottom() {
  setTimeout(() => {
    const _mainElement = document.getElementById("main");
    _mainElement?.scrollTo(0, _mainElement?.scrollHeight);
  }, 0);
}
