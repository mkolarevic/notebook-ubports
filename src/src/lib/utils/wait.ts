/**
 * Pauses execution for a specified amount of time, then optionally executes a provided callback function
 *
 * @param duration
 * Wait duration in miliseconds
 * @param callback
 * Optional function to execute after waiting
 */
export async function wait(duration: number = 0, callback: Function = null) {
  return new Promise((resolve) =>
    setTimeout(() => {
      if (callback) {
        resolve(callback?.());
      } else {
        resolve(undefined);
      }
    }, duration)
  );
}
