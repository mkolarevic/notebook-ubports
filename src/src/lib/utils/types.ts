export type SortByType = "createdAt" | "updatedAt" | "viewedAt";

export type AppSettingsType = {
  sortBy: SortByType;
  inverseSortOrder: boolean;
  appTheme: "system" | "light" | "dark";
  locale: LocaleType;
  baseFontSize: "small" | "normal" | "large" | "extraLarge";
  notesPerPage: 10 | 25 | 50 | 100;
  dateTimeFormat: "absolute" | "relative";
};

export type NotesPerPage = `${AppSettingsType["notesPerPage"]}`;

export type NoteObject = {
  // Data
  title: string;
  content: string;
  // Flags
  pinned: boolean;
  labels: string[];
  archived: boolean;
  // Metadata
  uuid: string;
  createdAt: number;
  updatedAt: number;
  viewedAt: number;
  deleted: boolean;
};

export type SortByOptions = "createdAt" | "updatedAt" | "viewedAt";

export type NoteExportFormat = "json" | "text" | "markdown";

export type LocaleType =
  | "arr"
  | "aussie"
  | "de"
  | "en"
  | "es"
  | "fr"
  | "hi"
  | "hr"
  | "hrg"
  | "it"
  | "ja"
  | "ko"
  | "pt_BR"
  | "pt"
  | "ru"
  | "sr"
  | "srl"
  | "tr"
  | "zh";

export type DateTimeFormatterParams = {
  date: Date | number | string;
  locale?: string;
  relative?: boolean | "absolute" | "relative";
};
