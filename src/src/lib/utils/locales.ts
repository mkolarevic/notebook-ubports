import { get, writable } from "svelte/store";
import { getAppSettings, setAppSettings } from "../state/storage";
import type { LocaleType } from "./types";

/* Private */
/** Returns a locale object depending on the user's device/renderer locale */
function _getDefaultLanguage(): typeof en {
  if (typeof navigator === "undefined") {
    return en;
  } else {
    function getLangString(lang: string) {
      // Map to alternative
      if (["bs", "sh"].includes(lang)) {
        return {
          "2": "hr",
          "5": "hr",
        };
      }
      if (["be", "uk"].includes(lang)) {
        return {
          "2": "ru",
          "5": "ru",
        };
      }

      return {
        "2": lang.substring(0, 2),
        "5": lang.substring(0, 5).replace("-", "_"),
      };
    }
    // Get the primary language
    const userLanguage = getLangString(navigator.language);

    // Return the localization strings for the primary language, if it is supported
    if (AllLocales[userLanguage[5]]) {
      return AllLocales[userLanguage[5]];
    }
    // Return the localization strings for the primary language, if it is supported
    if (AllLocales[userLanguage[2]]) {
      return AllLocales[userLanguage[2]];
    }

    // If the primary language is not supported, find other user-preferred languages
    let selectedLanguage = "en";
    navigator.languages.find((lang) => {
      const _l = getLangString(lang);

      if (AllLocales[_l[5]]) {
        selectedLanguage = _l[5];
        return true;
      }

      if (AllLocales[_l[2]]) {
        selectedLanguage = _l[2];
        return true;
      }
      return false;
    });

    // Return the first found user-preferred language. If it's unsupported, return English localizations.
    return AllLocales[selectedLanguage] ?? en;
  }
}

/* Locales */
export function SupportedLocales(): { [x in keyof typeof AllLocales]: string } {
  const _localeOptions = {};
  const _keys = Object.keys(AllLocales) as (keyof typeof AllLocales)[];

  for (let i = 0; i < _keys.length; i++) {
    _localeOptions[_keys[i]] = AllLocales[_keys[i]]._language;
  }

  return _localeOptions as { [x in keyof typeof AllLocales]: string };
}

const arr: typeof en = {
  _locale: "arr",
  _language: "English (Pirate)",
  note: {
    note: "Scroll",
    title: "Title",
  },
  noteList: {
    noArchivedNotes: "Ye don't have any scrolls in the treasury",
    noArchivedNotesText: `Ye may put a scroll in the treasury by openin' it, then pressin' the "Put in treasury" choice within the port.`,
    noNotes: "Ye don't have any scrolls",
    noNotesFound: "No scrolls found",
    noNotesFoundAction: "Belay scouting",
    noNotesText: `Write yer scroll by pressin' the "cross" flag in the bottom right.`,
  },
  sidebar: {
    addLabel: "Mark",
    archive: "Put in treasury",
    archived: "Treasury",
    createdAt: "Written at",
    delete: "Burn this here scroll",
    deleteSingleContent:
      "If ye burn this here scroll, ye will not be able to salvage it.",
    deleteSingleTitle: "Burn scroll?",
    exportOne: "Plunder",
    exportOneDescription: "Choose yer preferred file format.",
    filter: "Filter",
    labels: "Marks",
    manage: "Handle scrolls",
    menu: "Menu",
    notes: "Scrolls",
    options: "Choices",
    pin: "Pin",
    selectAFileFormat: "Select a file format",
    settings: "Fancies",
    sort: "Sort",
    sortBy: "Order by",
    sortOrder: "Sort order",
    sortOrderAsc: "ascendin'",
    sortOrderDesc: "descendin'",
    unarchive: "Remove from treasury",
    unpin: "Unpin",
    updatedAt: "Amended at",
    viewedAt: "Discerned at",
  },
  dialog: {
    cancel: "Belay",
    close: "Furl",
    confirm: "Aye!",
  },
  navbar: {
    search: "Scout",
  },
  labelDialog: {
    inputLabel: "New mark",
    newLabelName: "New mark name",
    renameLabel: "Rename mark",
    selectLabels: "Choose marks",
  },
  labels: {
    deleteContent: "Do ye wish to remove the mark:",
    deleteTitle: "Remove the mark?",
    labeledNotes: "Marked scrolls:",
    noLabels: "No marks",
    noLabelsText: `Ye may create marks by openin' a scroll, then pressin' the "New mark" choice within the port.`,
  },
  settings: {
    about: "Particulars",
    absolute: "Landlubber time",
    clearAllData: "Davey Jones' locker",
    componentLibrary: "Ship parts constructed by SMUI",
    darkTheme: "Night colors",
    data: "Treasure",
    deleteAllContent:
      "This here will burn all scrolls, includin' all scrolls in the treasury. All marks be removed as well.",
    deleteAllTitle: "Burn all scrolls?",
    developedBy: "Ship's captain: {0}",
    exportAll: "Plunder everythin'",
    exportAllDescription:
      "Choose yer preferred file format. Warnin': ye be plunderin' all scrolls, includin' the scrolls in the treasury.",
    extraLarge: "Gargantuan",
    fontSize: "Scribble size",
    interface: "Shipshape",
    language: "Tongue",
    large: "Hearty",
    lightTheme: "Day colors",
    normal: "Proper",
    pageSize: "Scrolls per page",
    poweredBySvelte: "Run by Svelte",
    relative: "Dead reckoning",
    small: "Wee",
    systemTheme: "Captain's colors",
    theme: "Colors",
    timeFormat: "Time format",
  },
  time: {
    justNow: "This moment",
  },
};

const de: typeof en = {
  _locale: "de",
  _language: "Deutsch",
  note: {
    note: "Notiz",
    title: "Titel",
  },
  noteList: {
    noArchivedNotes: "Du hast keine archivierten Notizen",
    noArchivedNotesText: `Du kannst eine Notiz archivieren, indem du sie öffnest und dann die Option "Archivieren" in der Seitenleiste auswählst.`,
    noNotes: "Du hast keine Notizen",
    noNotesFound: "Keine Notizen gefunden",
    noNotesFoundAction: "Suche löschen",
    noNotesText: `Erstelle eine neue Notiz, indem du auf die Schaltfläche "+" in der unteren rechten Ecke klickst.`,
  },
  sidebar: {
    addLabel: "Label hinzufügen",
    archive: "Archivieren",
    archived: "Archiviert",
    createdAt: "Erstellt am",
    delete: "Löschen",
    deleteSingleContent:
      "Wenn du diese Notiz löschst, kannst du sie nicht wiederherstellen.",
    deleteSingleTitle: "Notiz löschen?",
    exportOne: "Exportieren",
    exportOneDescription: "Wähle das bevorzugte Dateiformat aus.",
    filter: "Filter",
    labels: "Labels",
    manage: "Verwalten",
    menu: "Menü",
    notes: "Notizen",
    options: "Optionen",
    pin: "Anheften",
    selectAFileFormat: "Dateiformat auswählen",
    settings: "Einstellungen",
    sort: "Sortieren",
    sortBy: "Sortieren nach",
    sortOrder: "Sortierreihenfolge",
    sortOrderAsc: "aufsteigend",
    sortOrderDesc: "absteigend",
    unarchive: "Aus Archiv entfernen",
    unpin: "Abheften",
    updatedAt: "Aktualisiert am",
    viewedAt: "Angesehen am",
  },
  dialog: {
    cancel: "Abbrechen",
    close: "Schließen",
    confirm: "Bestätigen",
  },
  navbar: {
    search: "Suche",
  },
  labelDialog: {
    inputLabel: "Neues Label",
    newLabelName: "Neuer Labelname",
    renameLabel: "Label umbenennen",
    selectLabels: "Labels auswählen",
  },
  labels: {
    deleteContent: "Bestätige, wenn du das Label löschen möchtest:",
    deleteTitle: "Label löschen?",
    labeledNotes: "Gekennzeichnete Notizen:",
    noLabels: "Keine Labels",
    noLabelsText: `Du kannst Labels erstellen, indem du eine Notiz öffnest und dann die Option "Neues Label" in der Seitenleiste auswählst.`,
  },
  settings: {
    about: "Über",
    absolute: "Absolut",
    clearAllData: "Alle Daten löschen",
    componentLibrary: "Komponentenbibliothek bereitgestellt von SMUI",
    darkTheme: "Dunkles Thema",
    data: "Daten",
    deleteAllContent:
      "Dadurch werden alle Notizen gelöscht, einschließlich aller archivierten Notizen. Alle Labels werden ebenfalls entfernt.",
    deleteAllTitle: "Alle Daten löschen?",
    developedBy: "Entwickelt von {0}",
    exportAll: "Alle exportieren",
    exportAllDescription:
      "Wähle das bevorzugte Dateiformat aus. Hinweis: Du exportierst alle Notizen, einschließlich archivierter Notizen.",
    extraLarge: "Extra groß",
    fontSize: "Schriftgröße",
    interface: "Benutzeroberfläche",
    language: "Sprache",
    large: "Groß",
    lightTheme: "Helles Thema",
    normal: "Normal",
    pageSize: "Notizen pro Seite",
    poweredBySvelte: "Angetrieben von Svelte",
    relative: "Relativ",
    small: "Klein",
    systemTheme: "Systemthema",
    theme: "Thema",
    timeFormat: "Zeitformat",
  },
  time: {
    justNow: "Gerade eben",
  },
};

const en = {
  /** Locale string of the language */
  _locale: "en" as LocaleType,
  /** Name of the language, in it's own locale */
  _language: "English",
  note: {
    /** Placeholder for the Note Content input field */
    note: "Note",
    /** Placeholder for the Note Title input field */
    title: "Title",
  },
  noteList: {
    /** Page title which displays on the Archived Notes page, when the user has no notes in the `archived` state */
    noArchivedNotes: "You don't have any archived notes",
    /** Page content which displays on the Archived Notes page, when the user has no notes in the `archived` state */
    noArchivedNotesText: `You can archive a note by opening it, then pressing the "Archive" option within the sidebar.`,
    /** Page title which displays on the Notes page, when the user has no notes */
    noNotes: "You don't have any notes",
    /** Page title which displays on the Notes page, when the user has made a search but no matching notes have been found */
    noNotesFound: "No notes found",
    /** CTA button used to clear the search/filter, displayed when the user has made a search but no matching notes have been found */
    noNotesFoundAction: "Clear search",
    /** Page content which displays on the Notes page, when the user has no notes */
    noNotesText: `Create a new note by pressing the "plus" button in the bottom right.`,
  },
  sidebar: {
    /** Action Button which lets the user add a new label to the opened note */
    addLabel: "Add label",
    /** Action Button which moves the note into the `archived` state */
    archive: "Archive",
    /** Link to the Archived Notes page */
    archived: "Archived",
    /** Action Button which displays the sorting method for notes. In this case, notes are sorted by the "Created at" date/time */
    createdAt: "Created at",
    /** Action Button which permanently deletes the note */
    delete: "Delete",
    /** Content shown in a warning dialog/modal when the user attempts to delete a note */
    deleteSingleContent:
      "If you delete this note, you will not be able to recover it.",
    /** Title shown in a warning dialog/modal when the user attempts to delete a note */
    deleteSingleTitle: "Delete note?",
    /** Action Button which lets the user save a note as a file */
    exportOne: "Export",
    /** Content of the confirmation dialog/modal shown when the user tries to save a note as a file */
    exportOneDescription: "Please choose your preferred file format.",
    /** Label for the sidebar section containing filter buttons */
    filter: "Filter",
    /** Link to the Labels page */
    labels: "Labels",
    /** Label for the sidebar section containing action buttons used to manage the note */
    manage: "Manage",
    /** Title of the sidebar */
    menu: "Menu",
    /** Link to the Notes page, containing all the non-archived notes */
    notes: "Notes",
    /** Label for the sidebar section containing option buttons */
    options: "Options",
    /** Action Button which toggles the `pinned` state of the note to `true` */
    pin: "Pin",
    /** Title of the confirmation dialog/modal shown when the user tries to save a note as a file */
    selectAFileFormat: "Select a file format",
    /** Link to the Settings page */
    settings: "Settings",
    /** Label for the sidebar section containing sorting options */
    sort: "Sort",
    /** Action button used to change the sort parameter */
    sortBy: "Sort by",
    /** Action button used to change the sort order */
    sortOrder: "Sort order",
    /** Ascending sort order */
    sortOrderAsc: "ascending",
    /** Descending sort order */
    sortOrderDesc: "descending",
    /** Action Button which moves the note out of the `archived` state */
    unarchive: "Unarchive",
    /** Action Button which toggles the `pinned` state of the note to `false */
    unpin: "Unpin",
    /** Action Button which displays the sorting method for notes. In this case, notes are sorted by the "Updated at" date/time */
    updatedAt: "Updated at",
    /** Action Button which displays the sorting method for notes. In this case, notes are sorted by the "Viewed at" date/time */
    viewedAt: "Viewed at",
  },
  dialog: {
    /** Button for canceling actions, displayed in confirmation dialogs/modals */
    cancel: "Cancel",
    /** Button for closing dialogs/modals */
    close: "Close",
    /** Button for confirming actions, displayed in confirmation dialogs/modals */
    confirm: "Confirm",
  },
  navbar: {
    /** Placeholder displayed in the Search input field used to search for notes */
    search: "Search",
  },
  labelDialog: {
    /** Placeholder displayed in the input field used for creating a new label */
    inputLabel: "New label",
    /** Placeholder displayed in the input field used for renaming a label */
    newLabelName: "New label name",
    /** Title of the dialog/modal used for renaming a label */
    renameLabel: "Rename label",
    /** Title of the dialog/modal used for managing which labels are assigned to a note */
    selectLabels: "Select labels",
  },
  labels: {
    /** Content of the confirmation dialog/modal when the user attempts to delete a label */
    deleteContent: "Confirm if you want to delete the label:",
    /** Title of the confirmation dialog/modal when the user attempts to delete a label */
    deleteTitle: "Delete the label?",
    /** Indicator used to display how many notes have a specific label */
    labeledNotes: "Labeled notes:",
    /** Page title which displays on the Labels page, when the user has no labels */
    noLabels: "No labels",
    /** Page content which displays on the Labels page, when the user has no labels */
    noLabelsText: `You can create labels by opening a note, then selecting the "New label" option within the sidebar.`,
  },
  settings: {
    /** Button used to display information about the application */
    about: "About",
    /** Time format selection, in this case "Absolute" (for example: 21/01/2021, 10:15) */
    absolute: "Absolute",
    /** Action button which deletes all stored data from the application */
    clearAllData: "Clear all data",
    /** Credits to the SMUI component library */
    componentLibrary: "Component library provided by SMUI",
    /** Theme selection, in this case "dark" theme */
    darkTheme: "Dark theme",
    /** Settings page label for the section related to data operations, such as export or clear */
    data: "Data",
    /** Content of the confirmation dialog/modal shown when the user tries to delete all data from the application */
    deleteAllContent:
      "This will delete all notes, including all archived notes. All tags will be removed as well.",
    /** Title of the confirmation dialog/modal shown when the user tries to delete all data from the application */
    deleteAllTitle: "Delete all data?",
    /** Displays the developer name */
    developedBy: "Developed by {0}",
    /** Action button used to export all notes into a file */
    exportAll: "Export all",
    /** Content of the confirmation dialog which is shown when the user attempts to export notes into a file */
    exportAllDescription:
      "Please choose your preferred file format. Note: You are exporting all notes, including archived notes.",
    /** Font size - extra large */
    extraLarge: "Extra large",
    /** Label for the font size selection/dropdown input */
    fontSize: "Font size",
    /** Settings page label for the section related to interface options, such as theming, language, font size, etc. */
    interface: "Interface",
    /** Label for the language selection/dropdown input */
    language: "Language",
    /** Font size - large */
    large: "Large",
    /** Theme selection, in this case "light" theme */
    lightTheme: "Light theme",
    /** Font size - normal/standard/default */
    normal: "Normal",
    /** Label for the pagination selection/dropdown input */
    pageSize: "Notes per page",
    /** Credits to the Svelte JS library */
    poweredBySvelte: "Powered by Svelte",
    /** Time format selection, in this case "Relative" (for example: 15 minutes ago) */
    relative: "Relative",
    /** Font size - small */
    small: "Small",
    /** Theme selection, in this case "system" theme decided by the user device */
    systemTheme: "System theme",
    /** Label for the theme selection/dropdown input */
    theme: "Theme",
    /** Label for the time format selection/dropdown input */
    timeFormat: "Time format",
  },
  time: {
    /** "Just now" or "A moment ago" - displays when the relative time difference is less than 1 second */
    justNow: "Just now",
  },
};

const es: typeof en = {
  _locale: "es",
  _language: "Español",
  note: {
    note: "Nota",
    title: "Título",
  },
  noteList: {
    noArchivedNotes: "No tienes notas archivadas",
    noArchivedNotesText: `Puedes archivar una nota abriéndola y luego presionando la opción "Archivar" en la barra lateral.`,
    noNotes: "No tienes ninguna nota",
    noNotesFound: "No se encontraron notas",
    noNotesFoundAction: "Limpiar búsqueda",
    noNotesText: `Crea una nueva nota presionando el botón "+" en la esquina inferior derecha.`,
  },
  sidebar: {
    addLabel: "Agregar etiqueta",
    archive: "Archivar",
    archived: "Archivadas",
    createdAt: "Creado",
    delete: "Eliminar",
    deleteSingleContent: "Si eliminas esta nota, no podrás recuperarla.",
    deleteSingleTitle: "¿Eliminar nota?",
    exportOne: "Exportar",
    exportOneDescription: "Seleccione el formato de archivo preferido.",
    filter: "Filtro",
    labels: "Etiquetas",
    manage: "Gestionar",
    menu: "Menú",
    notes: "Notas",
    options: "Opciones",
    pin: "Fijar",
    selectAFileFormat: "Seleccionar formato de archivo",
    settings: "Ajustes",
    sort: "Ordenar",
    sortBy: "Ordenar por",
    sortOrder: "Orden de clasificación",
    sortOrderAsc: "ascendente",
    sortOrderDesc: "descendente",
    unarchive: "Desarchivar",
    unpin: "Desfijar",
    updatedAt: "Actualizado",
    viewedAt: "Visto",
  },
  dialog: {
    cancel: "Cancelar",
    close: "Cerrar",
    confirm: "Confirmar",
  },
  navbar: {
    search: "Buscar",
  },
  labelDialog: {
    inputLabel: "Nueva etiqueta",
    newLabelName: "Nuevo nombre de etiqueta",
    renameLabel: "Renombrar etiqueta",
    selectLabels: "Seleccionar etiquetas",
  },
  labels: {
    deleteContent: "Confirma si deseas eliminar la etiqueta:",
    deleteTitle: "¿Eliminar etiqueta?",
    labeledNotes: "Notas etiquetadas:",
    noLabels: "No hay etiquetas",
    noLabelsText: `Puedes crear etiquetas abriendo una nota y luego seleccionando la opción "Nueva etiqueta" en la barra lateral.`,
  },
  settings: {
    about: "Acerca de",
    absolute: "Absoluto",
    clearAllData: "Eliminar todos los datos",
    componentLibrary: "Biblioteca de componentes proporcionada por SMUI",
    darkTheme: "Tema oscuro",
    data: "Datos",
    deleteAllContent:
      "Esto eliminará todas las notas, incluidas las archivadas. También se eliminarán todas las etiquetas.",
    deleteAllTitle: "¿Eliminar todos los datos?",
    developedBy: "Desarrollado por {0}",
    exportAll: "Exportar todas",
    exportAllDescription:
      "Seleccione el formato de archivo preferido. Nota: Estás exportando todas las notas, incluidas las archivadas.",
    extraLarge: "Extra grande",
    fontSize: "Tamaño de fuente",
    interface: "Interfaz",
    language: "Idioma",
    large: "Grande",
    lightTheme: "Tema claro",
    normal: "Normal",
    pageSize: "Notas por página",
    poweredBySvelte: "Desarrollado con Svelte",
    relative: "Relativo",
    small: "Pequeño",
    systemTheme: "Tema del sistema",
    theme: "Tema",
    timeFormat: "Formato de hora",
  },
  time: {
    justNow: "Justo ahora",
  },
};

const fr: typeof en = {
  _locale: "fr",
  _language: "Français",
  note: {
    note: "Note",
    title: "Titre",
  },
  noteList: {
    noArchivedNotes: "Vous n'avez aucune note archivée",
    noArchivedNotesText: `Vous pouvez archiver une note en l'ouvrant, puis en sélectionnant l'option "Archiver" dans la barre latérale.`,
    noNotes: "Vous n'avez aucune note",
    noNotesFound: "Aucune note trouvée",
    noNotesFoundAction: "Effacer la recherche",
    noNotesText: `Créez une nouvelle note en appuyant sur le bouton "+" dans le coin inférieur droit.`,
  },
  sidebar: {
    addLabel: "Ajouter une étiquette",
    archive: "Archiver",
    archived: "Archivées",
    createdAt: "Créé à",
    delete: "Supprimer",
    deleteSingleContent:
      "Si vous supprimez cette note, vous ne pourrez pas la récupérer.",
    deleteSingleTitle: "Supprimer la note ?",
    exportOne: "Exporter",
    exportOneDescription: "Veuillez choisir le format de fichier souhaité.",
    filter: "Filtrer",
    labels: "Étiquettes",
    manage: "Gérer",
    menu: "Menu",
    notes: "Notes",
    options: "Options",
    pin: "Épingler",
    selectAFileFormat: "Sélectionner un format de fichier",
    settings: "Paramètres",
    sort: "Trier",
    sortBy: "Trier par",
    sortOrder: "Ordre de tri",
    sortOrderAsc: "ascendant",
    sortOrderDesc: "descendant",
    unarchive: "Désarchiver",
    unpin: "Désépingler",
    updatedAt: "Mis à jour à",
    viewedAt: "Vu à",
  },
  dialog: {
    cancel: "Annuler",
    close: "Fermer",
    confirm: "Confirmer",
  },
  navbar: {
    search: "Rechercher",
  },
  labelDialog: {
    inputLabel: "Nouvelle étiquette",
    newLabelName: "Nouveau nom d'étiquette",
    renameLabel: "Renommer l'étiquette",
    selectLabels: "Sélectionner des étiquettes",
  },
  labels: {
    deleteContent: "Confirmez si vous souhaitez supprimer l'étiquette :",
    deleteTitle: "Supprimer l'étiquette ?",
    labeledNotes: "Notes étiquetées :",
    noLabels: "Aucune étiquette",
    noLabelsText: `Vous pouvez créer des étiquettes en ouvrant une note, puis en sélectionnant l'option "Nouvelle étiquette" dans la barre latérale.`,
  },
  settings: {
    about: "À propos",
    absolute: "Absolu",
    clearAllData: "Effacer toutes les données",
    componentLibrary: "Bibliothèque de composants fournie par SMUI",
    darkTheme: "Thème sombre",
    data: "Données",
    deleteAllContent:
      "Cela supprimera toutes les notes, y compris toutes les notes archivées. Toutes les étiquettes seront également supprimées.",
    deleteAllTitle: "Supprimer toutes les données ?",
    developedBy: "Développé par {0}",
    exportAll: "Tout exporter",
    exportAllDescription:
      "Veuillez choisir le format de fichier souhaité. Remarque : Vous exportez toutes les notes, y compris les notes archivées.",
    extraLarge: "Très grand",
    fontSize: "Taille de police",
    interface: "Interface",
    language: "Langue",
    large: "Grand",
    lightTheme: "Thème clair",
    normal: "Normal",
    pageSize: "Notes par page",
    poweredBySvelte: "Propulsé par Svelte",
    relative: "Relatif",
    small: "Petit",
    systemTheme: "Thème du système",
    theme: "Thème",
    timeFormat: "Format de l'heure",
  },
  time: {
    justNow: "À l'instant",
  },
};

const hi: typeof en = {
  _locale: "hi",
  _language: "हिन्दी",
  note: {
    note: "नोट",
    title: "शीर्षक",
  },
  noteList: {
    noArchivedNotes: "आपके पास कोई संग्रहीत नोट नहीं हैं",
    noArchivedNotesText: `आप एक नोट को संग्रहीत कर सकते हैं, इसे खोलकर, फिर साइडबार में "संग्रहीत करें" विकल्प का चयन करके।`,
    noNotes: "आपके पास कोई नोट नहीं हैं",
    noNotesFound: "कोई नोट नहीं मिली",
    noNotesFoundAction: "खोज को हटाएं",
    noNotesText: `नई नोट बनाएं, "प्लस" बटन पर क्लिक करके।`,
  },
  sidebar: {
    addLabel: "लेबल जोड़ें",
    archive: "संग्रहीत करें",
    archived: "संग्रहीत",
    createdAt: "बनाया गया",
    delete: "हटाएं",
    deleteSingleContent:
      "यदि आप इस नोट को हटाते हैं, तो आप इसे पुनः प्राप्त नहीं कर पाएंगे।",
    deleteSingleTitle: "नोट हटाएं?",
    exportOne: "निर्यात करें",
    exportOneDescription: "कृपया पसंदीदा फ़ाइल प्रारूप का चयन करें।",
    filter: "फिल्टर",
    labels: "लेबल",
    manage: "प्रबंधित करें",
    menu: "मेनू",
    notes: "नोट",
    options: "विकल्प",
    pin: "पिन करें",
    selectAFileFormat: "फ़ाइल प्रारूप चुनें",
    settings: "सेटिंग्स",
    sort: "क्रमबद्ध",
    sortBy: "इसके द्वारा क्रमबद्ध करें",
    sortOrder: "क्रमबद्ध क्रम",
    sortOrderAsc: "आरोही",
    sortOrderDesc: "अवरोही",
    unarchive: "असंग्रहीत करें",
    unpin: "पिन हटाएं",
    updatedAt: "अपडेट किया गया",
    viewedAt: "देखा गया",
  },
  dialog: {
    cancel: "रद्द करें",
    close: "बंद करें",
    confirm: "पुष्टि करें",
  },
  navbar: {
    search: "खोजें",
  },
  labelDialog: {
    inputLabel: "नया लेबल",
    newLabelName: "नया लेबल नाम",
    renameLabel: "लेबल का नाम बदलें",
    selectLabels: "लेबल चुनें",
  },
  labels: {
    deleteContent: "यदि आप लेबल को हटाना चाहते हैं, तो कृपया पुष्टि करें:",
    deleteTitle: "लेबल हटाएं?",
    labeledNotes: "लेबल लगाए गए नोट:",
    noLabels: "कोई लेबल नहीं",
    noLabelsText: `आप एक नोट खोलकर "नया लेबल" विकल्प का चयन करके लेबल बना सकते हैं।`,
  },
  settings: {
    about: "के बारे में",
    absolute: "निरपेक्ष",
    clearAllData: "सभी डेटा हटाएं",
    componentLibrary: "SMUI द्वारा प्रदान किया गया घटक पुस्तकालय",
    darkTheme: "डार्क थीम",
    data: "डेटा",
    deleteAllContent:
      "यह सभी नोटों को हटा देगा, सभी संग्रहीत नोटों सहित। सभी टैग भी हटा दिए जाएंगे।",
    deleteAllTitle: "सभी डेटा हटाएं?",
    developedBy: "द्वारा विकसित: {0}",
    exportAll: "सभी निर्यात करें",
    exportAllDescription:
      "कृपया पसंदीदा फ़ाइल प्रारूप का चयन करें। ध्यान दें: आप सभी नोटों को, संग्रहीत नोटों सहित, निर्यात कर रहे हैं।",
    extraLarge: "अत्यधिक बड़ा",
    fontSize: "फ़ॉन्ट का आकार",
    interface: "अंतराफलक",
    language: "भाषा",
    large: "बड़ा",
    lightTheme: "प्रकाशमान थीम",
    normal: "सामान्य",
    pageSize: "पृष्ठ पर नोट",
    poweredBySvelte: "Svelte द्वारा संचालित",
    relative: "सांविक",
    small: "छोटा",
    systemTheme: "सिस्टम थीम",
    theme: "थीम",
    timeFormat: "समय प्रारूप",
  },
  time: {
    justNow: "अभी-अभी",
  },
};

const hr: typeof en = {
  _locale: "hr",
  _language: "Hrvatski",
  note: {
    note: "Bilješka",
    title: "Naslov",
  },
  noteList: {
    noArchivedNotes: "Nemate arhiviranih bilješki",
    noArchivedNotesText: `Možete arhivirati bilješku otvaranjem iste, a zatim pritiskanjem opcije "Arhiviraj" unutar bočne trake.`,
    noNotes: "Nemate nijednu bilješku",
    noNotesFound: "Nema pronađenih bilješki",
    noNotesFoundAction: "Očisti pretragu",
    noNotesText: `Kreirajte novu bilješku pritiskom na gumb "plus" u donjem desnom kutu.`,
  },
  sidebar: {
    addLabel: "Dodaj oznaku",
    archive: "Arhiviraj",
    archived: "Arhivirane",
    createdAt: "Stvoreno",
    delete: "Obriši",
    deleteSingleContent: "Ako izbrišete ovu bilješku, nećete je moći vratiti.",
    deleteSingleTitle: "Izbriši bilješku?",
    exportOne: "Spremi",
    exportOneDescription: "Molimo odaberite željeni format datoteke.",
    filter: "Filtriraj",
    labels: "Oznake",
    manage: "Upravljanje",
    menu: "Izbornik",
    notes: "Bilješke",
    options: "Opcije",
    pin: "Zakači",
    selectAFileFormat: "Odaberite format datoteke",
    settings: "Postavke",
    sort: "Sortiraj",
    sortBy: "Sortiraj po",
    sortOrder: "Redoslijed sortiranja",
    sortOrderAsc: "uzlazno",
    sortOrderDesc: "silazno",
    unarchive: "Ukloni iz arhive",
    unpin: "Otkvači",
    updatedAt: "Ažurirano",
    viewedAt: "Pregledano",
  },
  dialog: {
    cancel: "Odustani",
    close: "Zatvori",
    confirm: "Potvrdi",
  },
  navbar: {
    search: "Pretraži",
  },
  labelDialog: {
    inputLabel: "Nova oznaka",
    newLabelName: "Novo ime oznake",
    renameLabel: "Preimenuj oznaku",
    selectLabels: "Odaberi oznake",
  },
  labels: {
    deleteContent: "Potvrdite ako želite izbrisati oznaku:",
    deleteTitle: "Izbriši oznaku?",
    labeledNotes: "Označene bilješke:",
    noLabels: "Nema oznaka",
    noLabelsText: `Možete kreirati oznake otvaranjem bilješke, a zatim odabirom opcije "Nova oznaka" unutar bočne trake.`,
  },
  settings: {
    about: "O aplikaciji",
    absolute: "Apsolutno",
    clearAllData: "Obriši sve podatke",
    componentLibrary: "Biblioteka komponenata pružena od strane SMUI",
    darkTheme: "Tamna tema",
    data: "Podaci",
    deleteAllContent:
      "Ovo će izbrisati sve bilješke, uključujući sve arhivirane bilješke. Sve oznake će također biti uklonjene.",
    deleteAllTitle: "Izbriši sve podatke?",
    developedBy: "Razvio {0}",
    exportAll: "Spremi sve",
    exportAllDescription:
      "Molimo odaberite željeni format datoteke. Napomena: Spremit ćete sve bilješke, uključujući arhivirane bilješke.",
    extraLarge: "Vrlo veliko",
    fontSize: "Veličina fonta",
    interface: "Sučelje",
    language: "Jezik",
    large: "Veliko",
    lightTheme: "Svijetla tema",
    normal: "Normalno",
    pageSize: "Bilješke po stranici",
    poweredBySvelte: "Ovu aplikaciju pokreće Svelte",
    relative: "Relativno",
    small: "Malo",
    systemTheme: "Tema sustava",
    theme: "Tema",
    timeFormat: "Format vremena",
  },
  time: {
    justNow: "Sada",
  },
};

const hrg: typeof en = {
  _locale: "hrg",
  _language: "Ⱈⱃⰲⰰⱅⱄⰽⰻ (Ⰳⰾⰰⰳⱁⰾⰻⱌⰰ)",
  note: {
    note: "Ⰱⰻⰾⰵⱋⰽⰰ",
    title: "Ⱀⰰⱄⰾⱁⰲ",
  },
  noteList: {
    noArchivedNotes: "Ⱀⰵⰿⰰⱅⰵ ⰰⱃⱈⰻⰲⰻⱃⰰⱀⰻⱈ ⰱⰻⰾⰵⱋⰽⰻ",
    noArchivedNotesText: `Ⰿⱁⰶⰵⱅⰵ ⰰⱃⱈⰻⰲⰻⱃⰰⱅⰻ ⰱⰻⰾⰵⱋⰽⱆ ⱁⱅⰲⰰⱃⰰњⰵⰿ ⰻⱄⱅⰵ, ⰰ ⰸⰰⱅⰻⰿ ⱂⱃⰻⱅⰻⱄⰽⰰњⰵⰿ ⱁⱂⱌⰻⰻⰵ "Ⰰⱃⱈⰻⰲⰻⱃⰰⰻ" ⱆⱀⱆⱅⰰⱃ ⰱⱁⱍⱀⰵ ⱅⱃⰰⰽⰵ.`,
    noNotes: "Ⱀⰵⰿⰰⱅⰵ ⱀⰻⰻⰵⰴⱀⱆ ⰱⰻⰾⰵⱋⰽⱆ",
    noNotesFound: "Ⱀⰵⰿⰰ ⱂⱃⱁⱀⰰⰼⰵⱀⰻⱈ ⰱⰻⰾⰵⱋⰽⰻ",
    noNotesFoundAction: "Ⱁⱍⰻⱄⱅⰻ ⱂⱃⰵⱅⱃⰰⰳⱆ",
    noNotesText: `Ⰽⱃⰵⰻⱃⰰⰻⱅⰵ ⱀⱁⰲⱆ ⰱⰻⰾⰵⱋⰽⱆ ⱂⱃⰻⱅⰻⱄⰽⱁⰿ ⱀⰰ ⰳⱆⰿⰱ "ⱂⰾⱆⱄ" ⱆ ⰴⱁњⰵⰿ ⰴⰵⱄⱀⱁⰿ ⰽⱆⱅⱆ.`,
  },
  sidebar: {
    addLabel: "Ⰴⱁⰴⰰⰻ ⱁⰸⱀⰰⰽⱆ",
    archive: "Ⰰⱃⱈⰻⰲⰻⱃⰰⰻ",
    archived: "Ⰰⱃⱈⰻⰲⰻⱃⰰⱀⰵ",
    createdAt: "Ⱄⱅⰲⱁⱃⰵⱀⱁ",
    delete: "Ⱁⰱⱃⰻⱋⰻ",
    deleteSingleContent: "Ⰰⰽⱁ ⰻⰸⰱⱃⰻⱋⰵⱅⰵ ⱁⰲⱆ ⰱⰻⰾⰵⱋⰽⱆ, ⱀⰵⰼⰵⱅⰵ ⰻⰵ ⰿⱁⰼⰻ ⰲⱃⰰⱅⰻⱅⰻ.",
    deleteSingleTitle: "Ⰻⰸⰱⱃⰻⱋⰻ ⰱⰻⰾⰵⱋⰽⱆ?",
    exportOne: "Ⱄⱂⱃⰵⰿⰻ",
    exportOneDescription: "Ⰿⱁⰾⰻⰿⱁ ⱁⰴⰰⰱⰵⱃⰻⱅⰵ ⰶⰵⰾⰵⱀⰻ ⱇⱁⱃⰿⰰⱅ ⰴⰰⱅⱁⱅⰵⰽⰵ.",
    filter: "Ⱇⰻⰾⱅⱃⰻⱃⰰⰻ",
    labels: "Ⱁⰸⱀⰰⰽⰵ",
    manage: "Ⱆⱂⱃⰰⰲⰾⰰњⰵ",
    menu: "Ⰻⰸⰱⱁⱃⱀⰻⰽ",
    notes: "Ⰱⰻⰾⰵⱋⰽⰵ",
    options: "Ⱁⱂⱌⰻⰻⰵ",
    pin: "Ⰸⰰⰽⰰⱍⰻ",
    selectAFileFormat: "Ⱁⰴⰰⰱⰵⱃⰻⱅⰵ ⱇⱁⱃⰿⰰⱅ ⰴⰰⱅⱁⱅⰵⰽⰵ",
    settings: "Ⱂⱁⱄⱅⰰⰲⰽⰵ",
    sort: "Ⱄⱁⱃⱅⰻⱃⰰⰻ",
    sortBy: "Ⱄⱁⱃⱅⰻⱃⰰⰻ ⱂⱁ",
    sortOrder: "Ⱃⰵⰴⱁⱄⰾⰻⰻⰵⰴ ⱄⱁⱃⱅⰻⱃⰰњⰰ",
    sortOrderAsc: "ⱆⰸⰾⰰⰸⱀⱁ",
    sortOrderDesc: "ⱄⰻⰾⰰⰸⱀⱁ",
    unarchive: "Ⱆⰽⰾⱁⱀⰻ ⰻⰸ ⰰⱃⱈⰻⰲⰵ",
    unpin: "Ⱁⱅⰽⰲⰰⱍⰻ",
    updatedAt: "Ⰰⰶⱆⱃⰻⱃⰰⱀⱁ",
    viewedAt: "Ⱂⱃⰵⰳⰾⰵⰴⰰⱀⱁ",
  },
  dialog: {
    cancel: "Ⱁⰴⱆⱄⱅⰰⱀⰻ",
    close: "Ⰸⰰⱅⰲⱁⱃⰻ",
    confirm: "Ⱂⱁⱅⰲⱃⰴⰻ",
  },
  navbar: {
    search: "Ⱂⱃⰵⱅⱃⰰⰶⰻ",
  },
  labelDialog: {
    inputLabel: "Ⱀⱁⰲⰰ ⱁⰸⱀⰰⰽⰰ",
    newLabelName: "Ⱀⱁⰲⱁ ⰻⰿⰵ ⱁⰸⱀⰰⰽⰵ",
    renameLabel: "Ⱂⱃⰵⰻⰿⰵⱀⱆⰻ ⱁⰸⱀⰰⰽⱆ",
    selectLabels: "Ⱁⰴⰰⰱⰵⱃⰻ ⱁⰸⱀⰰⰽⰵ",
  },
  labels: {
    deleteContent: "Ⱂⱁⱅⰲⱃⰴⰻⱅⰵ ⰰⰽⱁ ⰶⰵⰾⰻⱅⰵ ⰻⰸⰱⱃⰻⱄⰰⱅⰻ ⱁⰸⱀⰰⰽⱆ:",
    deleteTitle: "Ⰻⰸⰱⱃⰻⱋⰻ ⱁⰸⱀⰰⰽⱆ?",
    labeledNotes: "Ⱁⰸⱀⰰⱍⰵⱀⰵ ⰱⰻⰾⰵⱋⰽⰵ:",
    noLabels: "Ⱀⰵⰿⰰ ⱁⰸⱀⰰⰽⰰ",
    noLabelsText: `Ⰿⱁⰶⰵⱅⰵ ⰽⱃⰵⰻⱃⰰⱅⰻ ⱁⰸⱀⰰⰽⰵ ⱁⱅⰲⰰⱃⰰњⰵⰿ ⰱⰻⰾⰵⱋⰽⰵ, ⰰ ⰸⰰⱅⰻⰿ ⱁⰴⰰⰱⰻⱃⱁⰿ ⱁⱂⱌⰻⰻⰵ "Ⱀⱁⰲⰰ ⱁⰸⱀⰰⰽⰰ" ⱆⱀⱆⱅⰰⱃ ⰱⱁⱍⱀⰵ ⱅⱃⰰⰽⰵ.`,
  },
  settings: {
    about: "Ⱁ ⰰⱂⰾⰻⰽⰰⱌⰻⰻⰻ",
    absolute: "Ⰰⱂⱄⱁⰾⱆⱅⱀⱁ",
    clearAllData: "Ⱁⰱⱃⰻⱋⰻ ⱄⰲⰵ ⱂⱁⰴⰰⱅⰽⰵ",
    componentLibrary: "Ⰱⰻⰱⰾⰻⱁⱅⰵⰽⰰ ⰽⱁⰿⱂⱁⱀⰵⱀⰰⱅⰰ ⱂⱃⱆⰶⰵⱀⰰ ⱁⰴ ⱄⱅⱃⰰⱀⰵ ⰔⰏⰖⰋ",
    darkTheme: "Ⱅⰰⰿⱀⰰ ⱅⰵⰿⰰ",
    data: "Ⱂⱁⰴⰰⱌⰻ",
    deleteAllContent:
      "Ⱁⰲⱁ ⰼⰵ ⰻⰸⰱⱃⰻⱄⰰⱅⰻ ⱄⰲⰵ ⰱⰻⰾⰵⱋⰽⰵ, ⱆⰽⰾⱆⱍⱆⰻⱆⰼⰻ ⱄⰲⰵ ⰰⱃⱈⰻⰲⰻⱃⰰⱀⰵ ⰱⰻⰾⰵⱋⰽⰵ. Ⱄⰲⰵ ⱁⰸⱀⰰⰽⰵ ⰼⰵ ⱅⰰⰽⱁⰼⰵⱃ ⰱⰻⱅⰻ ⱆⰽⰾⱁњⰵⱀⰵ.",
    deleteAllTitle: "Ⰻⰸⰱⱃⰻⱋⰻ ⱄⰲⰵ ⱂⱁⰴⰰⱅⰽⰵ?",
    developedBy: "Ⱃⰰⰸⰲⰻⱁ {0}",
    exportAll: "Ⱄⱂⱃⰵⰿⰻ ⱄⰲⰵ",
    exportAllDescription:
      "Ⰿⱁⰾⰻⰿⱁ ⱁⰴⰰⰱⰵⱃⰻⱅⰵ ⰶⰵⰾⰵⱀⰻ ⱇⱁⱃⰿⰰⱅ ⰴⰰⱅⱁⱅⰵⰽⰵ. Ⱀⰰⱂⱁⰿⰵⱀⰰ: Ⱄⱂⱃⰵⰿⰻⱅ ⰼⰵⱅⰵ ⱄⰲⰵ ⰱⰻⰾⰵⱋⰽⰵ, ⱆⰽⰾⱆⱍⱆⰻⱆⰼⰻ ⰰⱃⱈⰻⰲⰻⱃⰰⱀⰵ ⰱⰻⰾⰵⱋⰽⰵ.",
    extraLarge: "Ⰲⱃⰾⱁ ⰲⰵⰾⰻⰽⱁ",
    fontSize: "Ⰲⰵⰾⰻⱍⰻⱀⰰ ⱇⱁⱀⱅⰰ",
    interface: "Ⱄⱆⱍⰵⰾⰵ",
    language: "Ⰻⰵⰸⰻⰽ",
    large: "Ⰲⰵⰾⰻⰽⱁ",
    lightTheme: "Ⱄⰲⰻⰻⰵⱅⰾⰰ ⱅⰵⰿⰰ",
    normal: "Ⱀⱁⱃⰿⰰⰾⱀⱁ",
    pageSize: "Ⰱⰻⰾⰵⱋⰽⰵ ⱂⱁ ⱄⱅⱃⰰⱀⰻⱌⰻ",
    poweredBySvelte: "Ⱁⰲⱆ ⰰⱂⰾⰻⰽⰰⱌⰻⰻⱆ ⱂⱁⰽⱃⰵⰼⰵ Ⱄⰲⰵⰾⱅⰵ",
    relative: "Ⱃⰵⰾⰰⱅⰻⰲⱀⱁ",
    small: "Ⰿⰰⰾⱁ",
    systemTheme: "Ⱅⰵⰿⰰ ⱄⱆⱄⱅⰰⰲⰰ",
    theme: "Ⱅⰵⰿⰰ",
    timeFormat: "Ⱇⱁⱃⰿⰰⱅ ⰲⱃⰵⰿⰵⱀⰰ",
  },
  time: {
    justNow: "Ⱄⰰⰴⰰ",
  },
};

const it: typeof en = {
  _locale: "it",
  _language: "Italiano",
  note: {
    note: "Nota",
    title: "Titolo",
  },
  noteList: {
    noArchivedNotes: "Non hai alcuna nota archiviata",
    noArchivedNotesText: `Puoi archiviare una nota aprendola e selezionando l'opzione "Archivia" nella barra laterale.`,
    noNotes: "Non hai alcuna nota",
    noNotesFound: "Nessuna nota trovata",
    noNotesFoundAction: "Cancella ricerca",
    noNotesText: `Crea una nuova nota premendo il pulsante "+" nell'angolo in basso a destra.`,
  },
  sidebar: {
    addLabel: "Aggiungi etichetta",
    archive: "Archivia",
    archived: "Archiviate",
    createdAt: "Creato il",
    delete: "Elimina",
    deleteSingleContent:
      "Se elimini questa nota, non sarà possibile recuperarla.",
    deleteSingleTitle: "Eliminare la nota?",
    exportOne: "Esporta",
    exportOneDescription: "Seleziona il formato di file preferito.",
    filter: "Filtra",
    labels: "Etichette",
    manage: "Gestisci",
    menu: "Menu",
    notes: "Note",
    options: "Opzioni",
    pin: "Punta",
    selectAFileFormat: "Seleziona un formato di file",
    settings: "Impostazioni",
    sort: "Ordina",
    sortBy: "Ordina per",
    sortOrder: "Ordinamento",
    sortOrderAsc: "crescente",
    sortOrderDesc: "decrescente",
    unarchive: "Disarchivia",
    unpin: "Non puntare",
    updatedAt: "Aggiornato il",
    viewedAt: "Visualizzato il",
  },
  dialog: {
    cancel: "Annulla",
    close: "Chiudi",
    confirm: "Conferma",
  },
  navbar: {
    search: "Cerca",
  },
  labelDialog: {
    inputLabel: "Nuova etichetta",
    newLabelName: "Nuovo nome etichetta",
    renameLabel: "Rinomina etichetta",
    selectLabels: "Seleziona etichette",
  },
  labels: {
    deleteContent: "Conferma se desideri eliminare l'etichetta:",
    deleteTitle: "Eliminare l'etichetta?",
    labeledNotes: "Note etichettate:",
    noLabels: "Nessuna etichetta",
    noLabelsText: `Puoi creare etichette aprendo una nota e selezionando l'opzione "Nuova etichetta" nella barra laterale.`,
  },
  settings: {
    about: "Informazioni",
    absolute: "Assoluto",
    clearAllData: "Cancella tutti i dati",
    componentLibrary: "Libreria componenti fornita da SMUI",
    darkTheme: "Tema scuro",
    data: "Dati",
    deleteAllContent:
      "Ciò cancellerà tutte le note, comprese quelle archiviate. Tutte le etichette verranno rimosse.",
    deleteAllTitle: "Cancellare tutti i dati?",
    developedBy: "Sviluppato da {0}",
    exportAll: "Esporta tutto",
    exportAllDescription:
      "Seleziona il formato di file preferito. Nota: Stai esportando tutte le note, comprese quelle archiviate.",
    extraLarge: "Extra grande",
    fontSize: "Dimensione carattere",
    interface: "Interfaccia",
    language: "Lingua",
    large: "Grande",
    lightTheme: "Tema chiaro",
    normal: "Normale",
    pageSize: "Note per pagina",
    poweredBySvelte: "Alimentato da Svelte",
    relative: "Relativo",
    small: "Piccolo",
    systemTheme: "Tema di sistema",
    theme: "Tema",
    timeFormat: "Formato orario",
  },
  time: {
    justNow: "Proprio adesso",
  },
};

const ja: typeof en = {
  _locale: "ja",
  _language: "日本語",
  note: {
    note: "ノート",
    title: "タイトル",
  },
  noteList: {
    noArchivedNotes: "アーカイブされたノートはありません",
    noArchivedNotesText: `ノートを開き、サイドバーで「アーカイブ」オプションを選択すると、ノートをアーカイブできます。`,
    noNotes: "ノートはありません",
    noNotesFound: "ノートが見つかりません",
    noNotesFoundAction: "検索をクリア",
    noNotesText: `右下の「＋」ボタンを押して新しいノートを作成してみてください。`,
  },
  sidebar: {
    addLabel: "ラベルを追加",
    archive: "アーカイブ",
    archived: "アーカイブ済み",
    createdAt: "作成日時",
    delete: "削除",
    deleteSingleContent: "このノートを削除すると、元に戻すことはできません。",
    deleteSingleTitle: "ノートを削除しますか？",
    exportOne: "エクスポート",
    exportOneDescription: "お好みのファイル形式を選択してください。",
    filter: "フィルター",
    labels: "ラベル",
    manage: "管理",
    menu: "メニュー",
    notes: "ノート",
    options: "オプション",
    pin: "ピン留め",
    selectAFileFormat: "ファイル形式を選択",
    settings: "設定",
    sort: "ソート",
    sortBy: "次でソート",
    sortOrder: "ソート順",
    sortOrderAsc: "昇順",
    sortOrderDesc: "降順",
    unarchive: "アーカイブ解除",
    unpin: "ピン留め解除",
    updatedAt: "更新日時",
    viewedAt: "閲覧日時",
  },
  dialog: {
    cancel: "キャンセル",
    close: "閉じる",
    confirm: "確認",
  },
  navbar: {
    search: "検索",
  },
  labelDialog: {
    inputLabel: "新しいラベル",
    newLabelName: "新しいラベル名",
    renameLabel: "ラベル名を変更",
    selectLabels: "ラベルを選択",
  },
  labels: {
    deleteContent: "このラベルを削除しますか？",
    deleteTitle: "ラベルを削除しますか？",
    labeledNotes: "ラベル付きノート：",
    noLabels: "ラベルはありません",
    noLabelsText: `ノートを開き、サイドバーで「新しいラベル」オプションを選択してラベルを作成してみてください。`,
  },
  settings: {
    about: "情報",
    absolute: "絶対",
    clearAllData: "すべてのデータをクリア",
    componentLibrary: "SMUI コンポーネントライブラリによって提供されます",
    darkTheme: "ダークテーマ",
    data: "データ",
    deleteAllContent:
      "これにより、すべてのノートが削除されます。アーカイブされたすべてのノートも削除されます。すべてのラベルも削除されます。",
    deleteAllTitle: "すべてのデータを削除しますか？",
    developedBy: "開発者：{0}",
    exportAll: "すべてエクスポート",
    exportAllDescription:
      "お好みのファイル形式を選択してください。注：アーカイブされたノートを含むすべてのノートがエクスポートされます。",
    extraLarge: "超大型",
    fontSize: "フォントサイズ",
    interface: "インターフェース",
    language: "言語",
    large: "大",
    lightTheme: "ライトテーマ",
    normal: "通常",
    pageSize: "ページあたりのノート数",
    poweredBySvelte: "Svelte によって支えられています",
    relative: "相対",
    small: "小",
    systemTheme: "システムテーマ",
    theme: "テーマ",
    timeFormat: "時間形式",
  },
  time: {
    justNow: "たった今",
  },
};

const ko: typeof en = {
  _locale: "ko",
  _language: "한국어",
  note: {
    note: "노트",
    title: "제목",
  },
  noteList: {
    noArchivedNotes: "보관된 노트가 없습니다",
    noArchivedNotesText: `노트를 열고 사이드바에서 "보관" 옵션을 선택하여 노트를 보관할 수 있습니다.`,
    noNotes: "노트가 없습니다",
    noNotesFound: "찾는 노트가 없습니다",
    noNotesFoundAction: "검색 지우기",
    noNotesText: `우측 하단의 "+" 버튼을 눌러 새 노트를 만들어 보세요.`,
  },
  sidebar: {
    addLabel: "라벨 추가",
    archive: "보관",
    archived: "보관됨",
    createdAt: "생성 시간",
    delete: "삭제",
    deleteSingleContent: "이 노트를 삭제하면 복구할 수 없습니다.",
    deleteSingleTitle: "노트 삭제?",
    exportOne: "내보내기",
    exportOneDescription: "선호하는 파일 형식을 선택하세요.",
    filter: "필터",
    labels: "라벨",
    manage: "관리",
    menu: "메뉴",
    notes: "노트",
    options: "옵션",
    pin: "고정",
    selectAFileFormat: "파일 형식 선택",
    settings: "설정",
    sort: "정렬",
    sortBy: "다음으로 정렬",
    sortOrder: "정렬 순서",
    sortOrderAsc: "오름차순",
    sortOrderDesc: "내림차순",
    unarchive: "보관 해제",
    unpin: "고정 해제",
    updatedAt: "업데이트 시간",
    viewedAt: "열람 시간",
  },
  dialog: {
    cancel: "취소",
    close: "닫기",
    confirm: "확인",
  },
  navbar: {
    search: "검색",
  },
  labelDialog: {
    inputLabel: "새 라벨",
    newLabelName: "새 라벨 이름",
    renameLabel: "라벨 이름 변경",
    selectLabels: "라벨 선택",
  },
  labels: {
    deleteContent: "라벨을 삭제하시겠습니까?",
    deleteTitle: "라벨 삭제?",
    labeledNotes: "라벨이 지정된 노트:",
    noLabels: "라벨 없음",
    noLabelsText: `노트를 열고 사이드바에서 "새 라벨" 옵션을 선택하여 라벨을 만들어 보세요.`,
  },
  settings: {
    about: "정보",
    absolute: "절대",
    clearAllData: "모든 데이터 지우기",
    componentLibrary: "SMUI 컴포넌트 라이브러리로 제공됨",
    darkTheme: "다크 테마",
    data: "데이터",
    deleteAllContent:
      "이 작업은 모든 노트를 삭제하며, 보관된 모든 노트도 삭제됩니다. 모든 태그도 제거됩니다.",
    deleteAllTitle: "모든 데이터 삭제?",
    developedBy: "개발자: {0}",
    exportAll: "모두 내보내기",
    exportAllDescription:
      "선호하는 파일 형식을 선택하세요. 참고: 보관된 노트를 포함하여 모든 노트가 내보내집니다.",
    extraLarge: "매우 큼",
    fontSize: "글꼴 크기",
    interface: "인터페이스",
    language: "언어",
    large: "크게",
    lightTheme: "라이트 테마",
    normal: "보통",
    pageSize: "페이지당 노트 수",
    poweredBySvelte: "Svelte로 제작됨",
    relative: "상대적",
    small: "작게",
    systemTheme: "시스템 테마",
    theme: "테마",
    timeFormat: "시간 형식",
  },
  time: {
    justNow: "방금",
  },
};

const pt_BR: typeof en = {
  _locale: "pt_BR",
  _language: "Português (Brazil)",
  note: {
    note: "Nota",
    title: "Título",
  },
  noteList: {
    noArchivedNotes: "Você não tem notas arquivadas",
    noArchivedNotesText: `Você pode arquivar uma nota abrindo-a e selecionando a opção "Arquivar" na barra lateral.`,
    noNotes: "Você não tem notas",
    noNotesFound: "Nenhuma nota encontrada",
    noNotesFoundAction: "Limpar pesquisa",
    noNotesText: `Crie uma nova nota pressionando o botão "+" no canto inferior direito.`,
  },
  sidebar: {
    addLabel: "Adicionar rótulo",
    archive: "Arquivar",
    archived: "Arquivadas",
    createdAt: "Criado em",
    delete: "Excluir",
    deleteSingleContent:
      "Se você excluir esta nota, não será possível recuperá-la.",
    deleteSingleTitle: "Excluir nota?",
    exportOne: "Exportar",
    exportOneDescription: "Selecione o formato de arquivo preferido.",
    filter: "Filtrar",
    labels: "Etiquetas",
    manage: "Gerenciar",
    menu: "Menu",
    notes: "Notas",
    options: "Opções",
    pin: "Fixar",
    selectAFileFormat: "Selecionar um formato de arquivo",
    settings: "Configurações",
    sort: "Ordenar",
    sortBy: "Ordenar por",
    sortOrder: "Ordem de classificação",
    sortOrderAsc: "crescente",
    sortOrderDesc: "decrescente",
    unarchive: "Desarquivar",
    unpin: "Desafixar",
    updatedAt: "Atualizado em",
    viewedAt: "Visualizado em",
  },
  dialog: {
    cancel: "Cancelar",
    close: "Fechar",
    confirm: "Confirmar",
  },
  navbar: {
    search: "Buscar",
  },
  labelDialog: {
    inputLabel: "Novo rótulo",
    newLabelName: "Novo nome do rótulo",
    renameLabel: "Renomear rótulo",
    selectLabels: "Selecionar etiquetas",
  },
  labels: {
    deleteContent: "Confirme se deseja excluir o rótulo:",
    deleteTitle: "Excluir rótulo?",
    labeledNotes: "Notas etiquetadas:",
    noLabels: "Sem etiquetas",
    noLabelsText: `Você pode criar etiquetas abrindo uma nota e selecionando a opção "Novo rótulo" na barra lateral.`,
  },
  settings: {
    about: "Sobre",
    absolute: "Absoluto",
    clearAllData: "Limpar todos os dados",
    componentLibrary: "Biblioteca de componentes fornecida pela SMUI",
    darkTheme: "Tema escuro",
    data: "Dados",
    deleteAllContent:
      "Isso excluirá todas as notas, incluindo as arquivadas. Todas as etiquetas também serão removidas.",
    deleteAllTitle: "Excluir todos os dados?",
    developedBy: "Desenvolvido por {0}",
    exportAll: "Exportar tudo",
    exportAllDescription:
      "Selecione o formato de arquivo preferido. Observação: Você está exportando todas as notas, incluindo as arquivadas.",
    extraLarge: "Extra grande",
    fontSize: "Tamanho da fonte",
    interface: "Interface",
    language: "Idioma",
    large: "Grande",
    lightTheme: "Tema claro",
    normal: "Normal",
    pageSize: "Notas por página",
    poweredBySvelte: "Desenvolvido por Svelte",
    relative: "Relativo",
    small: "Pequeno",
    systemTheme: "Tema do sistema",
    theme: "Tema",
    timeFormat: "Formato de hora",
  },
  time: {
    justNow: "Agora mesmo",
  },
};

const pt: typeof en = {
  _locale: "pt",
  _language: "Português (Portugal)",
  note: {
    note: "Nota",
    title: "Título",
  },
  noteList: {
    noArchivedNotes: "Não tem notas arquivadas",
    noArchivedNotesText: `Pode arquivar uma nota abrindo-a e selecionando a opção "Arquivar" na barra lateral.`,
    noNotes: "Não tem notas",
    noNotesFound: "Nenhuma nota encontrada",
    noNotesFoundAction: "Limpar pesquisa",
    noNotesText: `Crie uma nova nota pressionando o botão "+" no canto inferior direito.`,
  },
  sidebar: {
    addLabel: "Adicionar etiqueta",
    archive: "Arquivar",
    archived: "Arquivadas",
    createdAt: "Criado em",
    delete: "Eliminar",
    deleteSingleContent:
      "Se eliminar esta nota, não será possível recuperá-la.",
    deleteSingleTitle: "Eliminar nota?",
    exportOne: "Exportar",
    exportOneDescription: "Selecione o formato de ficheiro preferido.",
    filter: "Filtrar",
    labels: "Etiquetas",
    manage: "Gerir",
    menu: "Menu",
    notes: "Notas",
    options: "Opções",
    pin: "Fixar",
    selectAFileFormat: "Selecionar um formato de ficheiro",
    settings: "Definições",
    sort: "Ordenar",
    sortBy: "Ordenar por",
    sortOrder: "Ordem de ordenação",
    sortOrderAsc: "crescente",
    sortOrderDesc: "decrescente",
    unarchive: "Desarquivar",
    unpin: "Desfixar",
    updatedAt: "Atualizado em",
    viewedAt: "Visualizado em",
  },
  dialog: {
    cancel: "Cancelar",
    close: "Fechar",
    confirm: "Confirmar",
  },
  navbar: {
    search: "Pesquisar",
  },
  labelDialog: {
    inputLabel: "Nova etiqueta",
    newLabelName: "Novo nome da etiqueta",
    renameLabel: "Renomear etiqueta",
    selectLabels: "Selecionar etiquetas",
  },
  labels: {
    deleteContent: "Confirme se deseja eliminar a etiqueta:",
    deleteTitle: "Eliminar etiqueta?",
    labeledNotes: "Notas etiquetadas:",
    noLabels: "Sem etiquetas",
    noLabelsText: `Pode criar etiquetas abrindo uma nota e selecionando a opção "Nova etiqueta" na barra lateral.`,
  },
  settings: {
    about: "Sobre",
    absolute: "Absoluto",
    clearAllData: "Limpar todos os dados",
    componentLibrary: "Biblioteca de componentes fornecida pela SMUI",
    darkTheme: "Tema escuro",
    data: "Dados",
    deleteAllContent:
      "Isto eliminará todas as notas, incluindo as arquivadas. Todas as etiquetas também serão removidas.",
    deleteAllTitle: "Eliminar todos os dados?",
    developedBy: "Desenvolvido por {0}",
    exportAll: "Exportar tudo",
    exportAllDescription:
      "Selecione o formato de ficheiro preferido. Nota: Está a exportar todas as notas, incluindo as arquivadas.",
    extraLarge: "Extra grande",
    fontSize: "Tamanho da fonte",
    interface: "Interface",
    language: "Idioma",
    large: "Grande",
    lightTheme: "Tema claro",
    normal: "Normal",
    pageSize: "Notas por página",
    poweredBySvelte: "Desenvolvido por Svelte",
    relative: "Relativo",
    small: "Pequeno",
    systemTheme: "Tema do sistema",
    theme: "Tema",
    timeFormat: "Formato de hora",
  },
  time: {
    justNow: "Agora mesmo",
  },
};

const ru: typeof en = {
  _locale: "ru",
  _language: "Русский",
  note: {
    note: "Заметка",
    title: "Заголовок",
  },
  noteList: {
    noArchivedNotes: "У вас нет архивированных заметок",
    noArchivedNotesText: `Вы можете архивировать заметку, открыв ее, а затем нажав на кнопку "Архивировать" в боковой панели.`,
    noNotes: "У вас нет заметок",
    noNotesFound: "Заметок не найдено",
    noNotesFoundAction: "Очистить поиск",
    noNotesText: `Создайте новую заметку, нажав на кнопку "плюс" в правом нижнем углу.`,
  },
  sidebar: {
    addLabel: "Добавить метку",
    archive: "Архивировать",
    archived: "Архив",
    createdAt: "Создано",
    delete: "Удалить",
    deleteSingleContent:
      "Если вы удалите эту заметку, вы не сможете ее восстановить.",
    deleteSingleTitle: "Удалить заметку?",
    exportOne: "Экспорт",
    exportOneDescription: "Выберите формат файла.",
    filter: "Фильтр",
    labels: "Метки",
    manage: "Управление",
    menu: "Меню",
    notes: "Заметки",
    options: "Настройки",
    pin: "Закрепить",
    selectAFileFormat: "Выберите формат файла",
    settings: "Настройки",
    sort: "Сортировка",
    sortBy: "Сортировать по",
    sortOrder: "Порядок сортировки",
    sortOrderAsc: "по возрастанию",
    sortOrderDesc: "по убыванию",
    unarchive: "Извлечь из архива",
    unpin: "Открепить",
    updatedAt: "Обновлено",
    viewedAt: "Просмотрено",
  },
  dialog: {
    cancel: "Отмена",
    close: "Закрыть",
    confirm: "Подтвердить",
  },
  navbar: {
    search: "Поиск",
  },
  labelDialog: {
    inputLabel: "Новая метка",
    newLabelName: "Новое имя метки",
    renameLabel: "Переименовать метку",
    selectLabels: "Выбрать метки",
  },
  labels: {
    deleteContent: "Подтвердите удаление метки:",
    deleteTitle: "Удалить метку?",
    labeledNotes: "Заметки с меткой:",
    noLabels: "Нет меток",
    noLabelsText: `Вы можете создать метки, открыв заметку, а затем выбрав опцию "Новая метка" в боковой панели.`,
  },
  settings: {
    about: "О приложении",
    absolute: "Абсолютное",
    clearAllData: "Очистить все данные",
    componentLibrary: "Библиотека компонентов предоставлена SMUI",
    darkTheme: "Темная тема",
    data: "Данные",
    deleteAllContent:
      "Это действие удалит все заметки, включая все архивированные. Все метки также будут удалены.",
    deleteAllTitle: "Удалить все данные?",
    developedBy: "Разработано {0}",
    exportAll: "Экспорт всего",
    exportAllDescription:
      "Выберите формат файла. Примечание: Вы экспортируете все заметки, включая архивированные.",
    extraLarge: "Очень крупный",
    fontSize: "Размер шрифта",
    interface: "Интерфейс",
    language: "Язык",
    large: "Крупный",
    lightTheme: "Светлая тема",
    normal: "Обычный",
    pageSize: "Заметок на странице",
    poweredBySvelte: "Приложение создано на Svelte",
    relative: "Относительное",
    small: "Маленький",
    systemTheme: "Системная тема",
    theme: "Тема",
    timeFormat: "Формат времени",
  },
  time: {
    justNow: "Только что",
  },
};

const sr: typeof en = {
  _locale: "sr",
  _language: "Српски",
  note: {
    note: "Белешка",
    title: "Наслов",
  },
  noteList: {
    noArchivedNotes: "Немате архивираних белешки",
    noArchivedNotesText: `Можете архивирати белешку отварањем исте, а затим притиском опције "Архивирај" унутар бочне траке.`,
    noNotes: "Немате ниједну белешку",
    noNotesFound: "Нема пронађених белешки",
    noNotesFoundAction: "Очисти претрагу",
    noNotesText: `Направите нову белешку притиском на дугме "plus" у донјем десном углу.`,
  },
  sidebar: {
    addLabel: "Додај ознаку",
    archive: "Архивирај",
    archived: "Архивиране",
    createdAt: "Створено",
    delete: "Обриши",
    deleteSingleContent: "Ако избришете ову белешку, нећете је моћи вратити.",
    deleteSingleTitle: "Обриши белешку?",
    exportOne: "Сачувај",
    exportOneDescription: "Молимо одаберите жељени формат датотеке.",
    filter: "Филтрирај",
    labels: "Ознаке",
    manage: "Управљање",
    menu: "Мени",
    notes: "Белешке",
    options: "Опције",
    pin: "Закачи",
    selectAFileFormat: "Одаберите формат датотеке",
    settings: "Поставке",
    sort: "Сортирај",
    sortBy: "Сортирај по",
    sortOrder: "Редослед сортирања",
    sortOrderAsc: "узлазно",
    sortOrderDesc: "силазно",
    unarchive: "Уклони из архиве",
    unpin: "Откваћи",
    updatedAt: "Ажурирано",
    viewedAt: "Прегледано",
  },
  dialog: {
    cancel: "Откажи",
    close: "Затвори",
    confirm: "Потврди",
  },
  navbar: {
    search: "Претражи",
  },
  labelDialog: {
    inputLabel: "Нова ознака",
    newLabelName: "Ново име ознаке",
    renameLabel: "Преименуј ознаку",
    selectLabels: "Одабери ознаке",
  },
  labels: {
    deleteContent: "Потврдите ако желите избрисати ознаку:",
    deleteTitle: "Избриши ознаку?",
    labeledNotes: "Означене белешке:",
    noLabels: "Нема ознака",
    noLabelsText: `Можете креирати ознаке отварањем белешке, а затим одабиром опције "Нова ознака" унутар бочне траке.`,
  },
  settings: {
    about: "О апликацији",
    absolute: "Апсолутно",
    clearAllData: "Обриши све податке",
    componentLibrary: "Библиотека компоненти пружена од стране SMUI",
    darkTheme: "Тамна тема",
    data: "Подаци",
    deleteAllContent:
      "Ово ће избрисати све белешке, укључујући све архивиране белешке. Све ознаке ће бити уклоњене.",
    deleteAllTitle: "Избриши све податке?",
    developedBy: "Развио {0}",
    exportAll: "Сачувај све",
    exportAllDescription:
      "Молимо одаберите жељени формат датотеке. Напомена: Сачувате све белешке, укључујући архивиране белешке.",
    extraLarge: "Врло велико",
    fontSize: "Величина фонта",
    interface: "Интерфејс",
    language: "Језик",
    large: "Велико",
    lightTheme: "Светла тема",
    normal: "Нормално",
    pageSize: "Белешке по страници",
    poweredBySvelte: "Ову апликацију покреће Svelte",
    relative: "Релативно",
    small: "Мало",
    systemTheme: "Тема система",
    theme: "Тема",
    timeFormat: "Формат времена",
  },
  time: {
    justNow: "Сада",
  },
};

const srl: typeof en = {
  _locale: "srl",
  _language: "Srpski",
  note: {
    note: "Beleška",
    title: "Naslov",
  },
  noteList: {
    noArchivedNotes: "Nemate arhiviranih beleški",
    noArchivedNotesText: `Možete arhivirati belešku otvaranjem iste, a zatim pritiskanjem opcije "Arhiviraj" unutar bočne trake.`,
    noNotes: "Nemate nijednu belešku",
    noNotesFound: "Nema pronađenih beleški",
    noNotesFoundAction: "Očisti pretragu",
    noNotesText: `Napravite novu belešku pritiskom na dugme "plus" u donjem desnom uglu.`,
  },
  sidebar: {
    addLabel: "Dodaj oznaku",
    archive: "Arhiviraj",
    archived: "Arhivirane",
    createdAt: "Stvoreno",
    delete: "Obriši",
    deleteSingleContent: "Ako izbrišete ovu belešku, nećete je moći vratiti.",
    deleteSingleTitle: "Obriši belešku?",
    exportOne: "Spremi",
    exportOneDescription: "Molimo odaberite željeni format datoteke.",
    filter: "Filtriraj",
    labels: "Oznake",
    manage: "Upravljanje",
    menu: "Meni",
    notes: "Beleške",
    options: "Opcije",
    pin: "Zakači",
    selectAFileFormat: "Odaberite format datoteke",
    settings: "Postavke",
    sort: "Sortiraj",
    sortBy: "Sortiraj po",
    sortOrder: "Redosled sortiranja",
    sortOrderAsc: "uzlazno",
    sortOrderDesc: "silazno",
    unarchive: "Ukloni iz arhive",
    unpin: "Otkvači",
    updatedAt: "Ažurirano",
    viewedAt: "Pregledano",
  },
  dialog: {
    cancel: "Otkaži",
    close: "Zatvori",
    confirm: "Potvrdi",
  },
  navbar: {
    search: "Pretraži",
  },
  labelDialog: {
    inputLabel: "Nova oznaka",
    newLabelName: "Novo ime oznake",
    renameLabel: "Preimenuj oznaku",
    selectLabels: "Odaberi oznake",
  },
  labels: {
    deleteContent: "Potvrdite ako želite izbrisati oznaku:",
    deleteTitle: "Izbriši oznaku?",
    labeledNotes: "Označene beleške:",
    noLabels: "Nema oznaka",
    noLabelsText: `Možete kreirati oznake otvaranjem beleške, a zatim odabirom opcije "Nova oznaka" unutar bočne trake.`,
  },
  settings: {
    about: "O aplikaciji",
    absolute: "Apsolutno",
    clearAllData: "Obriši sve podatke",
    componentLibrary: "Biblioteka komponenata pružena od strane SMUI",
    darkTheme: "Tamna tema",
    data: "Podaci",
    deleteAllContent:
      "Ovo će izbrisati sve beleške, uključujući sve arhivirane beleške. Sve oznake će biti uklonjene.",
    deleteAllTitle: "Izbriši sve podatke?",
    developedBy: "Razvio {0}",
    exportAll: "Spremi sve",
    exportAllDescription:
      "Molimo odaberite željeni format datoteke. Napomena: Spremate sve beleške, uključujući arhivirane beleške.",
    extraLarge: "Vrlo veliko",
    fontSize: "Veličina fonta",
    interface: "Interfejs",
    language: "Jezik",
    large: "Veliko",
    lightTheme: "Svetla tema",
    normal: "Normalno",
    pageSize: "Beleške po stranici",
    poweredBySvelte: "Ovu aplikaciju pokreće Svelte",
    relative: "Relativno",
    small: "Malo",
    systemTheme: "Tema sistema",
    theme: "Tema",
    timeFormat: "Format vremena",
  },
  time: {
    justNow: "Sada",
  },
};

const tr: typeof en = {
  _locale: "tr",
  _language: "Türkçe",
  note: {
    note: "Not",
    title: "Başlık",
  },
  noteList: {
    noArchivedNotes: "Arşivlenmiş notunuz bulunmamaktadır",
    noArchivedNotesText: `Notu arşivlemek için notu açın ve ardından yan menüden "Arşivle" seçeneğini seçin.`,
    noNotes: "Hiçbir notunuz bulunmamaktadır",
    noNotesFound: "Hiçbir not bulunamadı",
    noNotesFoundAction: "Aramayı temizle",
    noNotesText: `Yeni bir not oluşturmak için sağ alt köşedeki "+" düğmesine basın.`,
  },
  sidebar: {
    addLabel: "Etiket ekle",
    archive: "Arşivle",
    archived: "Arşivlenmiş",
    createdAt: "Oluşturulma tarihi",
    delete: "Sil",
    deleteSingleContent: "Bu notu silerseniz geri alamazsınız.",
    deleteSingleTitle: "Notu silmek istiyor musunuz?",
    exportOne: "Dışa aktar",
    exportOneDescription: "Tercih ettiğiniz dosya biçimini seçin.",
    filter: "Filtrele",
    labels: "Etiketler",
    manage: "Yönet",
    menu: "Menü",
    notes: "Notlar",
    options: "Seçenekler",
    pin: "Sabitle",
    selectAFileFormat: "Bir dosya biçimi seçin",
    settings: "Ayarlar",
    sort: "Sırala",
    sortBy: "Şuna göre sırala",
    sortOrder: "Sıralama düzeni",
    sortOrderAsc: "artan",
    sortOrderDesc: "azalan",
    unarchive: "Arşivden çıkar",
    unpin: "Sabitlemeyi kaldır",
    updatedAt: "Güncellenme tarihi",
    viewedAt: "Görüntülenme tarihi",
  },
  dialog: {
    cancel: "İptal",
    close: "Kapat",
    confirm: "Onayla",
  },
  navbar: {
    search: "Ara",
  },
  labelDialog: {
    inputLabel: "Yeni etiket",
    newLabelName: "Yeni etiket adı",
    renameLabel: "Etiketi yeniden adlandır",
    selectLabels: "Etiketleri seç",
  },
  labels: {
    deleteContent: "Etiketi silmek istediğinize emin misiniz?",
    deleteTitle: "Etiketi silmek istiyor musunuz?",
    labeledNotes: "Etiketli notlar:",
    noLabels: "Etiket bulunmamaktadır",
    noLabelsText: `Notu açarak ve yan menüden "Yeni etiket" seçeneğini seçerek etiketler oluşturabilirsiniz.`,
  },
  settings: {
    about: "Hakkında",
    absolute: "Mutlak",
    clearAllData: "Tüm verileri temizle",
    componentLibrary: "SMUI bileşen kütüphanesi tarafından sağlanmaktadır",
    darkTheme: "Koyu tema",
    data: "Veri",
    deleteAllContent:
      "Bu, arşivlenmiş tüm notlar da dahil olmak üzere tüm notları siler. Tüm etiketler de kaldırılacaktır.",
    deleteAllTitle: "Tüm verileri silmek istiyor musunuz?",
    developedBy: "Geliştirici: {0}",
    exportAll: "Tümünü dışa aktar",
    exportAllDescription:
      "Tercih ettiğiniz dosya biçimini seçin. Not: Arşivlenmiş notlar da dahil olmak üzere tüm notları dışa aktarıyorsunuz.",
    extraLarge: "Çok büyük",
    fontSize: "Yazı tipi boyutu",
    interface: "Arayüz",
    language: "Dil",
    large: "Büyük",
    lightTheme: "Açık tema",
    normal: "Normal",
    pageSize: "Sayfa başına not",
    poweredBySvelte: "Svelte tarafından desteklenmektedir",
    relative: "Göreceli",
    small: "Küçük",
    systemTheme: "Sistem teması",
    theme: "Tema",
    timeFormat: "Zaman biçimi",
  },
  time: {
    justNow: "Şu anda",
  },
};

const zh: typeof en = {
  _locale: "zh",
  _language: "中文（简体）",
  note: {
    note: "笔记",
    title: "标题",
  },
  noteList: {
    noArchivedNotes: "没有已存档的笔记",
    noArchivedNotesText: `您可以通过打开笔记，然后在侧边栏中选择“存档”选项来将笔记存档。`,
    noNotes: "没有笔记",
    noNotesFound: "找不到笔记",
    noNotesFoundAction: "清除搜索",
    noNotesText: `请通过点击右下角的“+”按钮创建新的笔记。`,
  },
  sidebar: {
    addLabel: "添加标签",
    archive: "存档",
    archived: "已存档",
    createdAt: "创建时间",
    delete: "删除",
    deleteSingleContent: "删除此笔记后将无法恢复。",
    deleteSingleTitle: "删除笔记？",
    exportOne: "导出",
    exportOneDescription: "请选择您喜欢的文件格式。",
    filter: "筛选",
    labels: "标签",
    manage: "管理",
    menu: "菜单",
    notes: "笔记",
    options: "选项",
    pin: "置顶",
    selectAFileFormat: "选择文件格式",
    settings: "设置",
    sort: "排序",
    sortBy: "排序方式",
    sortOrder: "排序顺序",
    sortOrderAsc: "升序",
    sortOrderDesc: "降序",
    unarchive: "取消存档",
    unpin: "取消置顶",
    updatedAt: "更新时间",
    viewedAt: "查看时间",
  },
  dialog: {
    cancel: "取消",
    close: "关闭",
    confirm: "确认",
  },
  navbar: {
    search: "搜索",
  },
  labelDialog: {
    inputLabel: "新标签",
    newLabelName: "新标签名称",
    renameLabel: "重命名标签",
    selectLabels: "选择标签",
  },
  labels: {
    deleteContent: "确定要删除此标签吗？",
    deleteTitle: "删除标签？",
    labeledNotes: "已标记的笔记：",
    noLabels: "没有标签",
    noLabelsText: `您可以通过打开笔记，然后在侧边栏中选择“新标签”选项来创建标签。`,
  },
  settings: {
    about: "关于",
    absolute: "绝对",
    clearAllData: "清除所有数据",
    componentLibrary: "由 SMUI 组件库提供支持",
    darkTheme: "暗色主题",
    data: "数据",
    deleteAllContent:
      "此操作将删除所有笔记，包括所有存档的笔记。所有标签也将被删除。",
    deleteAllTitle: "删除所有数据？",
    developedBy: "开发者：{0}",
    exportAll: "全部导出",
    exportAllDescription:
      "请选择您喜欢的文件格式。注意：您将导出所有笔记，包括存档的笔记。",
    extraLarge: "特大",
    fontSize: "字号",
    interface: "界面",
    language: "语言",
    large: "大",
    lightTheme: "亮色主题",
    normal: "正常",
    pageSize: "每页笔记数量",
    poweredBySvelte: "由 Svelte 提供支持",
    relative: "相对",
    small: "小",
    systemTheme: "系统主题",
    theme: "主题",
    timeFormat: "时间格式",
  },
  time: {
    justNow: "刚刚",
  },
};

const aussie = {
  ...en,
  _language: "English (Australian)",
  _locale: "aussie" as const,
};

const AllLocales = {
  de,
  en,
  arr,
  aussie,
  es,
  fr,
  hi,
  hr,
  hrg,
  it,
  ja,
  ko,
  pt,
  pt_BR,
  ru,
  sr,
  srl,
  tr,
  zh,
};

/* State */
export const L = writable(_getDefaultLanguage());

/**
 * Returns a locale string for the provided internal locale.
 * If the parameter is not specified, returns the locale string for current user locale `L`
 *
 * @param locale
 * Internal locale string
 */
export function getLocaleString(locale: LocaleType = get(L)._locale) {
  const localeMap = {
    arr: "en-GB",
    aussie: "en-AU",
    de: "de-DE",
    en: "en-US",
    es: "es-ES",
    fr: "fr-FR",
    hi: "hi-IN",
    hr: "hr-HR",
    hrg: "hr-HR",
    it: "it-IT",
    ja: "ja-JP",
    ko: "ko-KR",
    pt_BR: "pt-BR",
    pt: "pt-PT",
    ru: "ru-RU",
    sr: "sr-SP",
    srl: "sr-Latn-RS",
    tr: "tr-TR",
    zh: "zh-CN",
  } as const;

  return localeMap[locale];
}

/* Actions */
/**
 * Changes the application locale
 *
 * @param newLocale
 * Locale to switch to
 */
export function changeLocale(newLocale: LocaleType) {
  L.set(AllLocales[newLocale] ?? en);
  const _appSettings = getAppSettings();
  _appSettings.locale = newLocale;
  setAppSettings(_appSettings);

  if (newLocale === "aussie") {
    document.getElementById("app").classList.add("aussie");
  } else {
    document.getElementById("app").classList.remove("aussie");
  }
}
