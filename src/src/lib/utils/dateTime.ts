import { get } from "svelte/store";
import { L } from "./locales";
import type { DateTimeFormatterParams } from "./types";

const defaultDateTimeFormatOptions: Intl.DateTimeFormatOptions = {
  dateStyle: "long",
  timeStyle: "medium",
};

/**
 * Formats a provided date based on the provided options and parameters
 *
 * @param {Object} params
 * Object holding the parameters
 * @param {DateTimeFormatterParams['date']} params.date
 * Date of the event, in UNIX time
 * @param {DateTimeFormatterParams['locale']} [params.locale]
 * Locale used to format the date string
 * @param {DateTimeFormatterParams['relative']} [params.relative]
 * Determines if the relative time formatter should be used
 * @param options
 * @returns
 */
export function formatDate(
  params: DateTimeFormatterParams,
  options: Intl.DateTimeFormatOptions = defaultDateTimeFormatOptions
) {
  const _date = new Date(params.date).valueOf();

  if (["relative", true].includes(params.relative)) {
    return getRelativeTime(_date, params.locale);
  }

  return getAbsoluteTime(_date, params.locale, options);
}

/**
 * Returns a string representing a date, depending on the locale.
 *
 * @param date
 * Date of the event, in UNIX time
 * @param locale
 * Locale used to format the date string
 * @param options
 * Formatter options for `Intl.DateTimeFormat` function
 */
function getAbsoluteTime(
  date: number,
  locale?: string,
  options?: Intl.DateTimeFormatOptions
) {
  return Intl.DateTimeFormat(
    locale,
    options ?? {
      dateStyle: "full",
      timeStyle: "short",
    }
  ).format(date);
}

/**
 * Returns a string representing a date in relative time. For example; `"15 seconds ago"`, `"8 months ago"`, etc.
 *
 * @param date
 * Date of the event, in UNIX time
 * @param locale
 * Locale used to format the date string
 */
function getRelativeTime(date: number, locale?: string) {
  const _timeDiff = Math.floor((new Date().valueOf() - date) / 1000);
  const _formatter = new Intl.RelativeTimeFormat(locale);

  function _getRoundedTime(_p: keyof typeof period) {
    return Math.floor(-_timeDiff / period[_p]);
  }

  if (_timeDiff === 0) {
    return get(L).time.justNow;
  }

  if (_timeDiff < period.minutes) {
    return _formatter.format(-_timeDiff, "seconds");
  } else if (_timeDiff < period.hours) {
    return _formatter.format(_getRoundedTime("minutes"), "minutes");
  } else if (_timeDiff < period.days) {
    return _formatter.format(_getRoundedTime("hours"), "hours");
  } else if (_timeDiff < period.weeks) {
    return _formatter.format(_getRoundedTime("days"), "days");
  } else if (_timeDiff < period.months) {
    return _formatter.format(_getRoundedTime("weeks"), "weeks");
  } else if (_timeDiff < period.years) {
    return _formatter.format(_getRoundedTime("months"), "months");
  } else {
    return _formatter.format(_getRoundedTime("years"), "years");
  }
}

const period = {
  seconds: 1,
  minutes: 60,
  hours: 3600,
  days: 86400,
  weeks: 604800,
  months: 2419200,
  years: 29030400,
};
