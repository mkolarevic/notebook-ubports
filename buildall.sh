# Builds the Svelte project, copies the `dist` directory to the clickable app and builds the app

if test "$1" != "bun" && test "$1" != "npm" && test "$1" != "pnpm" && test "$1" != "yarn" && test "$1" != ""; then
  echo "Invalid argument. Please use one of: bun, npm, pnpm, or yarn"
  exit 1
fi

if [ -z "$1" ]; then
    PACKAGE_MANAGER="bun"
else
    PACKAGE_MANAGER="$1"
fi

cd src
"$PACKAGE_MANAGER" run build
"$PACKAGE_MANAGER" run export
cd ../app
clickable clean
clickable build
